<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="AnonyProxies.com - free anonymous Dark Web surfing, free anonymous proxies, free anonymous browsing & surfing, free anonymous mail, free anonymous proxies and VPN.">
    <meta name="author" content="AnonyProxies.com and https://anonyproxies.com/team.php">

    <title>AnonyProxies - Free web proxy, Free Anonymous VPN, Free anonymous surfing, IP protection, free anonymous dark web surfing, secret surfing.</title>

    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/grayscale.css" rel="stylesheet">
    <link href="static/css/jquery.modal.css" rel="stylesheet">
	
    <!-- modal CSS -->
    <style>
        p#1weeklyvpn-windows {
            display: none;
        }
        @media (max-width: 590px) {
            section#anonyproxy img {
                display: none;
            }
        }
        #modal-login {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 220px;
        }
        #modal-login input.form-control {
            min-width: 0;
            width: auto;
            display: inline;
            font-size: .9em;
        }
        #modal-1weeklyproxy-access {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 220px;
        }
        #modal-1weeklyproxy-access .form-control {
            min-width: 0;
            width: auto;
            display: inline;
            font-size: .9em;
        }
        #modal-1weeklyvpn-access {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 390px;
        }
        #modal-1weeklyvpn-access input.form-control {
            min-width: 0;
            width: auto;
            display: inline;
            font-size: .9em;
        }
        #modal-stripe {
            border-radius: 25px;
            display: none;
            min-width: 280px;
            background: #fff;
            min-height: 355px;
            background: rgb(255, 255, 255);
            /* Old browsers */
            background: -moz-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(255, 255, 255, 1)), color-stop(100%, rgba(229, 229, 229, 1)));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* IE10+ */
            background: linear-gradient(to bottom, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%);
            /* W3C */
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#e5e5e5', GradientType=0);
            /* IE6-9 */
        }
        #modal-stripe input.form-control {
            min-width: 0;
            width: auto;
            display: inline;
        }
    </style>

    <!-- Custom Fonts -->
    <link href="static/css/font.lora.css" rel="stylesheet" type="text/css">
    <link href="static/css/font.montserrat.css" rel="stylesheet" type="text/css">

    <!-- jQuery Version 1.11.0 -->
    <script src="static/js/jquery-1.11.0.js"></script>

    <!-- payment -->
    <script src="static/js/jquery.payment.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="static/js/bootstrap.min.js"></script>

    <!-- bpopup -->
    <script src="static/js/jquery.modal.js"></script>

    <!-- Plugin JavaScript -->
    <script src="static/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="static/js/grayscale.js"></script>

    <!-- Load the different js libraries for the different scripts -->
    <script src="/a2/main.js"></script>

    <script type="text/javascript">
        var AnonyProxies = this.AnonyProxies = {

            //modal init
            mirror: function () {
                var selection = $('#select-mirror');
                var option_select = selection.find('option:selected');
                data_hostname = option_select.attr('data-hostname');
                data_ip = option_select.attr('data-ip');
                var timer_loading = setTimeout(function () {
                    $('<option>loading...</option>').appendTo(selection);
                    selection.val('loading...');
                }, 1000);
                var timer_redirect = setTimeout(function () {
                    if ($('#ip-surf-checked').is(':checked'))
                        window.location = 'http://' + data_ip;
                    else
                        window.location = 'http://' + data_hostname;
                }, 2000);

            },

            modal_login: function () {

                if (window.localStorage["email"]) {
                    $('#anonyproxies_user_form_username').val(window.localStorage["email"]);
                }
                if (window.localStorage["password"]) {
                    $('#anonyproxies_user_form_password').val(window.localStorage["password"]);
                }
                $('#modal-login').bPopup();

            },

            modal_stripe: function (id, pricing, label, plan) {

                $(id).bPopup({
                    follow: false
                });
                $(id).find('.modal-pricing').html(pricing);
                $(id).find('.modal-label').html(label);
                $(id).find('#plan').val(plan);

            },

            //billing

            stripe_init_form: function () {

                $('input[data-stripe="number"]').payment('formatCardNumber');
                $('input[data-stripe="cvc"]').payment('formatCardCVC');

                $('input[data-stripe="number"]').blur(function () {
                    var card_type = $.payment.cardType($('input[data-stripe="number"]').val());
                    if (card_type)
                        $('.input-group-cc').html(card_type);
                });

                if (window.localStorage["1weeklyvpn-token"]) {
                    $('#btn-1weeklyvpn-access').show();
                    $('#btn-1weeklyvpn-subscribe').hide();
                }

                if (window.localStorage["1weeklyproxy-token"]) {
                    $('#btn-1weeklyproxy-access').show();
                    $('#btn-1weeklyproxy-subscribe').hide();
                }

                if (window.localStorage["1weeklypptp-token"]) {
                    $('#pptp-username').attr('type', 'text');
                    $('#pptp-password').attr('type', 'text');
                    $('#btn-1weeklypptp-subscribe').hide();
                }

                $('button#cc_submit').on('touchend', function (e) {
                    $('#anonyproxies_stripe').submit();
                });


                $('#anonyproxies_stripe').submit(function (e) {

                    // show card type: 
                    var card_type = $.payment.cardType($('input[data-stripe="number"]').val());
                    $('.input-group-cc').html(card_type);

                    // run the post
                    AnonyProxies.stripe(this, '#anonyproxies_stripe input[data-stripe="email"]', $('#plan').val());

                    return false;

                });

            },

            stripe: function (form_selector, email_field, plan) {

                var $form = $(form_selector);
                var number = $form.find('input[data-stripe="number"]').val();
                var email = $(email_field).val();

                $form.find('button').prop('disabled', true);

                // post to stripe get a card token back
                Stripe.card.createToken($form, function (status, response) {
                    if (response.error) {
                        // Show the errors on the form
                        alert(response.error.message);
                        $form.find('button').prop('disabled', false);
                    } else {
                        // response contains id and card, which contains additional card details
                        var token = response.id;
                        var last_four = number.slice(-4);

                        //generate a password
                        password = Math.floor(Math.random() * 9000) + 1000;

                        // Insert the token into the form so it gets submitted to the server
                        $form.append($('<input type="hidden" name="stripeToken" />').val(token));

                        $.post("/api/users/create_user_token/", {
                                "username": email,
                                "email": email,
                                "password": password,
                                "token": token,
                                "plan": plan,
                                "last_four": last_four,
                            },
                            function (data) {
                                if (data.success) {
                                    alert("Your username and password are now created!  Write down your password! \n\nUsername: " + email + "\nPassword: " + password);

                                    // store the token email and pass
                                    window.localStorage["token"] = token;
                                    window.localStorage["email"] = email;
                                    window.localStorage["password"] = data.password;
                                    window.localStorage["last_four"] = last_four;
                                    window.localStorage["user_stripe_customer_id"] = data.user_stripe_customer_id;
                                    window.localStorage["user_id"] = data.user_id;

                                    // store the token and password for the plan
                                    window.localStorage[plan + "-token"] = token;
                                    window.localStorage[plan + "-email"] = email;
                                    window.localStorage[plan + "-password"] = data.password;
                                    window.localStorage[plan + "-last_four"] = last_four;
                                    window.localStorage[plan + "-user_stripe_customer_id"] = data.user_stripe_customer_id;
                                    window.localStorage[plan + "-user_id"] = data.user_id;

                                    // close the modal
                                    $('.b-close').click();

                                    // launch the vpn details modal
                                    $('#modal-' + plan + '-access').bPopup({
                                        follow: false
                                    });

                                    // show the button
                                    $('#btn-' + plan + '-subscribe').hide();
                                    $('#btn-' + plan + '-access').show();
                                } else {
                                    alert(data.error_msg);
                                }



                            }
                        );




                        // and submit
                        // $form.get(0).submit();
                    }

                    //reset the card type:
                    $('.input-group-cc').html("<i class='glyphicon glyphicon-ok'></i>");

                });
                return false;
            },


            init_dropdown: function () {
                $(".dropdown-menu li a").click(function () {
                    var selText = $(this).text();
                    $('.dropdown-toggle').html(selText + ' <span class="caret"></span>');

                    // global url
                    var global_url = $('#form-global-url').val();

                    // conditionals
                    if (selText == "CGIProxy") {

                    }

                });


            },

            init_onsubmit: function () {
                var global_url = $('#form-global-url').val();
                var service_text = $('.dropdown-toggle').text().trim();
                var is_search = $('#form-global-search').is(':checked');

                var global_search_url = 'http://m.search.aol.com/search?s_it=topsearchbox.nrf&v_t=na&q=';

                if (service_text == 'CGIProxy') {
                    //set CGIProxy form to the URL value and submit it!
                    if (is_search) {
                        var search_string = encodeURI(global_search_url + global_url);
                        $('form#cgiproxy').find('input[name="URL"]').val(search_string);
                    } else {
                        $('form#cgiproxy').find('input[name="URL"]').val(global_url);
                    }
                    $('form#cgiproxy').submit();

                }
                if (service_text == 'A2') {
                    //set A2 form to the URL value and submit it!
                    if (is_search) {
                        var search_string = encodeURIComponent('https://duckduckgo.com/html/?q=' + global_url);
                        window.location = '/a2/index.php?q=' + search_string + '&hl=5c5';
                    } else {
                        window.location = '/a2/index.php?q=' + encodeURIComponent(global_url) + '&hl=5c5';
                    }

                }
                if (service_text == 'Glype') {
                    //set Glype form to the URL value and submit it!
                    if (is_search) {
                        var search_string = encodeURI(global_search_url + global_url);
                        $('form#glype').find('input[name="u"]').val(search_string);
                    } else {
                        $('form#glype').find('input[name="u"]').val(global_url);
                    }
                    $('form#glype').submit();
                }
                if (service_text == 'PHProxy') {
                    //set Glype form to the URL value and submit it!
                    if (is_search) {
                        var search_string = encodeURI(global_search_url + global_url);
                        $('form#phproxy').find('input[id="address_box"]').val(search_string);
                    } else {
                        $('form#phproxy').find('input[id="address_box"]').val(global_url);
                    }
                    $('form#phproxy').submit();
                }
                if (service_text == 'PHProxy++') {
                    //set Glype form to the URL value and submit it!
                    if (is_search) {
                        var search_string = encodeURI(global_search_url + global_url);
                        $('form#phproxy_plus_plus').find('input[name="u"]').val(search_string);
                    } else {
                        $('form#phproxy_plus_plus').find('input[name="u"]').val(global_url);
                    }
                    $('form#phproxy_plus_plus').submit();
                }
                if (service_text == 'v3.2b2') {
                    //set Glype form to the URL value and submit it!
                    if (is_search) {
                        var search_string = encodeURI(global_search_url + global_url);
                        $('form#v32b2').find('input[name="url"]').val(search_string);
                    } else {
                        $('form#v32b2').find('input[name="url"]').val(global_url);
                    }
                    $('form#v32b2').submit();
                }

                return false;
            },

            init_enter: function () {
                $('input#form-global-url').keypress(function (e) {
                    if (e.which == 13) {
                        $('form#global_form').submit();
                        return false; //<---- Add this line
                    }
                });

            },

            init_search: function () {

                $('input[type="checkbox"]#form-global-search').on('click', function (e) {
                    if ($(this).is(":checked")) {
                        $("#form-global-url").val("").focus();
                    } else {
                        $("#form-global-url").val("http://").focus();
                    }

                });

                $('span#form-global-search-span').on('click touchstart', function (e) {
                    if ($(this).find('input[type="checkbox"]').is(':checked') && $('input#form-global-url').val() != '') {
                        if (!$(this).is(':checked'))
                            AnonyProxies.init_onsubmit();
                    }
                });


            },

            init: function () {
                AnonyProxies.init_dropdown();
            }


        };

        $(document).ready(function () {
            AnonyProxies.init();
            AnonyProxies.init_enter();
            AnonyProxies.init_search();

            AnonyProxies.stripe_init_form();
            var timeoutId;
            $('.btn-circle').mousedown(function () {
                timeoutId = setTimeout(function () {
                    window.localStorage.clear();
                    alert('localstore cleared!');
                    window.location = 'index.html';
                }, 5000);
            }).bind('mouseup mouseleave', function () {
                clearTimeout(timeoutId);
            });

            var iOS = (navigator.userAgent.match(/iPad|iPhone|iPod/g) ? true : false);

            if (iOS) {
                $('.ios-hide').hide();
            }

        });

        function addUrl(url) {
            document.getElementById('address_box').value = url;
        }

        function showOptions() {
            document.getElementById('options').style.display = 'show';
        }

        function form_submit() {
            init_form_submit();
        }
    </script>

</head>

<body onload="" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container" style='padding-right: 0px;'>
            <div class="navbar-header">

                <button style='padding-top: 7px;' type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="glyphicon glyphicon-align-justify" style='color: #fff;'></i>
                </button>

                <!--<label id='ip-surf' style='float: right; margin: 15px 0px 0px 10px; font-size: .9em; font-weight:normal;'>
                    <input id='ip-surf-checked' type='checkbox' />IP<span id='ip-surf-mirror'>-mirror</span>
                </label>-->

                <select id='select-mirror' onchange='AnonyProxies.mirror();' style='float: right; margin: 16px 0px 0px 0px; max-width: 64px; color: #000; font-size: .85em;'>
                    <option>mirror</option>
                    <option data-ip="104.194.232.247" data-hostname="www.anonyproxies.com">www.anonyproxies.com (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.anonyproxi.es">www.anonyproxi.es (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.superherofm.com">www.superherofm.com (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.moshiachradio.com">www.moshiachradio.com (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.redeemeradio.com">www.redeemeradio.com (global) SSL: on</option>
                    <option data-ip="github.com" data-hostname="www.github.com/anonyproxies/anonyweb">+ [Add a mirror]</option>
                    <!-- 104.219.54.54 -->

                </select>



                <a class="navbar-brand page-scroll" href="#page-top" style=''>
                    <i style='font-size:.9em;' class="glyphicon glyphicon-lock"></i>  <span class="light">Anony</span>Proxies
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_login_link' onclick='AnonyProxies.modal_login(); return false;' href="#">Login</a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_about_link' href="#about">About</a>
                    </li>

                    <li>
                        <a class="page-scroll" href="#anonymailer">Anony<b>Mailer</b></a>
                    </li>

                    <li id='navbar-anonyproxy'>
                        <a class="page-scroll" href="#anonyproxy">Anony<b>Proxy</b></a>
                    </li>

                    <li>
                        <a class="page-scroll" href="#vpn">Anony<b>VPN</b></a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_contact_link' href="#contact">Contact</a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_terms_link' href="#terms">Terms</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <h1 class="brand-heading">Anony<span style='color:#000;'>Proxies</span></h1>
                        <p class="intro-text">

                            Surf the web (or dark web) with anonymous and private web proxies powered by A2, Glype, PHProxy and PHProxy++. Subject to <a href='#terms' class='page-scroll btn btn-default'>Terms</a> above the auspices of Golden Ticket, Ticket to Gold and Disabled to Gold.

                        </p>

                        <form id='global_form' action='#' onsubmit='AnonyProxies.init_onsubmit(); return false;' method='post'>

                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn dropdown-toggle" data-toggle="dropdown">A2 <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#" onclick='return false;'>A2</a>
                                        </li>
                                        <!--<li><a href="#" onclick='return false;'>CGIProxy</a>
                                        </li>-->
                                        <li><a href="#" onclick='return false;'>Glype</a>
                                        </li>
                                        <li><a href="#" onclick='return false;'>PHProxy</a>
                                        </li>
                                        <li><a href="#" onclick='return false;'>PHProxy++</a>
                                        </li>
                                    </ul>

                                </div>
                                <!-- /btn-group -->

                                <input type="text" class="form-control" id='form-global-url' value='http://'>
                                <span id='form-global-search-span' class="input-group-addon"><input id='form-global-search' type='checkbox' onclick='' /> Search</span>

                            </div>
                            <!-- /input-group -->



                            <button class='btn btn-gray' id="go" type="submit" style='margin-left: 0px; margin-top: 10px;'>Browse</button>

                            <!--
                            <a class='ios-hide' href="https://play.google.com/store/apps/details?id=com.phonegap.anonygap">
                                <img style='height: 35px; max-width: 150px; margin: 9px 0 0 5px;' alt="Android app on Google Play" src="/static/img/android.png" />
                            </a>

                            <a class='ios-hide' href="http://www.windowsphone.com/en-us/store/app/anonyproxies-web-proxy-vpn/1d1cfa2e-2d11-45c0-a8d7-fc1bbf1cd6dd">
                                <img style='height: 35px; max-width: 150px; margin: 9px 0 0 5px;' alt="Windows Phone app on Windows Phone Store" src="/static/img/windows.png" />
                            </a>
                            -->


                        </form>
                        
                        <?php include("templates/forms.html"); ?>

                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="glyphicon glyphicon-chevron-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="global" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Now serving anonymous proxies around the world.</h2>
                <img id='global-map' src='static/img/map.png' />
                <br/>
                <br/>

                <p>
                    <a href='https://anonyproxies.com' class='btn btn-default'>Global (SSL)</a>
                </p>

            </div>
        </div>
    </section>


    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About </h2>
                <p>
                    AnonyProxies was founded years ago with the simple goal of offering free website proxy access to anyone needing anonymous and private internet browsing through a remote IP (hidden IP) address.
                </p>
                <p>
                    We are excited to announce that we are now offering free anonymous and encrypted VPN connections to the world enabling secure browsing for conscientious causes.
                </p>
            </div>
        </div>
    </section>


    <section id="anonymailer" class="content-section text-center">
        <div class="anonymailer-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">

                    <form method='POST' action='/anonymailer.php'>
                        <h2>AnonyMailer</h2>
                        <p>Send anonymous emails, subject to <a href='#terms' class='page-scroll btn btn-default'>Terms</a>
                        </p>

                        <div class="input-group">
                            <span class="input-group-addon">From</span>
                            <input name='from' type="text" class="form-control" value="">
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                            <span class="input-group-addon">To</span>
                            <input name='to' type="text" class="form-control" required value="">
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                            <span class="input-group-addon">Subject</span>
                            <input name='subject' type="text" class="form-control" value="">
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                            <span class="input-group-addon">
						  	Body
						  </span>
                            <textarea name="message" class="form-control"></textarea>
                        </div>

                        <button class='btn btn-gray' id="go" type="submit" style='margin-left: 0px; margin-top: 10px;'>
                            Send
                        </button>


                    </form>

                </div>
            </div>
        </div>
    </section>


    <section id="anonyproxy" class="content-section text-center">
        <div class="proxy-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Anonymous Proxy access </h2>
                    <p id='1weeklyvpn-windows'>
                        <i>New</i>! Need a <b>Windows Phone Proxy</b>?

                    </p>
                    <p>
                        Access the Windows Phone Proxy settings under your "All Settings" section under "WLAN", select a network and enter the following settings:

                        <br/>
                        <br/>

                        <!--
                        <button id='btn-1weeklyproxy-subscribe' onclick='AnonyProxies.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "AnonyProxies <i>Windows Phone Proxy</i>", "1weeklyproxy"); return false;' class='btn btn-blue' type='button'>Subscribe for $1.33/week</button>
                        -->
                        <!--<button id='btn-1weeklyproxy-help' onclick='$("#anonyproxy-img").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>?</button>-->
                        <button id='btn-1weeklyproxy-access' style='display:;' onclick='$("#modal-1weeklyproxy-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Proxy Settings</button>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="vpn" class="content-section text-center">
        <div class="vpn-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>VPN </h2>

                    <!-- <p id='1weeklyvpn-windows'>
                        <i>New</i>! Need <b>Windows Phone VPN</b>?

                        <button id='btn-1weeklyvpn-subscribe' onclick='AnonyProxies.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "AnonyProxies <i>Windows Phone VPN</i>", "1weeklyvpn"); return false;' class='btn btn-blue' type='button'>Subscribe now</button>
                        <button id='btn-1weeklyvpn-access' style='display:;' onclick='$("#modal-1weeklyvpn-access").bPopup({follow: false}); return false;' class='btn btn-blue' type='button'>Windows Phone VPN Settings</button>

                    </p> -->

                    <p>Connect to the PPTP VPN server using the following credentials, subject to <a href='#terms' class='page-scroll btn btn-default'>Terms</a>
                    </p>

                    <div class="input-group">
                        <span class="input-group-addon">Hostname</span>
                        <input type="text" class="form-control" value="anonyproxies.com">
                    </div>

                    <div class="input-group" style='margin-top: 10px;'>
                        <span class="input-group-addon">Username</span>
                        <input id='pptp-username' type="text" class="form-control" value="anonyproxies">
                    </div>

                    <div class="input-group" style='margin-top: 10px;'>
                        <span class="input-group-addon">Password</span>
                        <input id='pptp-password' type="text" class="form-control" value="anonyproxies">
                    </div>

                    <div class="input-group" style='margin-top: 10px;'>
                        <span class="input-group-addon">
					  	<span class='glyphicon glyphicon-ok'></span>
                        </span>
                        <input type="text" class="form-control" value="Use Point-to-Point-Encryption (MPPE)">
                    </div>

                    <br/>

                    <!--<button id='btn-1weeklypptp-subscribe' onclick='AnonyProxies.modal_stripe("#modal-stripe", "Subscribe for $1.33/week", "AnonyProxies <i>PPTP VPN</i> for Android, iOS, Windows, Mac or Linux", "1weeklypptp"); return false;' class='btn btn-blue' type='button'>Unlock user &amp; password</button>-->

                </div>
            </div>
        </div>
    </section>
    
    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Open Source </h2>
                <p>
                    This project is open source! 
                    <a href='https://github.com/anonyproxies/anonyweb'>Clone the Github repository</a> and be added to our Mirrors list with ample traffice for you!  Attribution required :)
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://github.com/anonyproxies/anonyweb" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    
    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contact </h2>
                <p>Feel free to email us at <a href="mailto:founders@anonyproxies.com">founders@anonyproxies.com</a>!</p>
                <p><a href="mailto:founders@anonyproxies.com">founders@anonyproxies.com</a>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="mailto:founders@anonyproxies.com" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Email</span></a>
                        <a href="https://twitter.com/anonyproxies" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a> 
                        <a href="https://github.com/anonyproxies/anonyweb" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>

                </ul>
            </div>
        </div>
    </section>

    <?php include("templates/terms.html"); ?>
    
    <!--<div id="map"></div>-->

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>
                
                Copyright &copy; <a href='https://www.anonyproxies.com'>AnonyProxies.com</a>: free web proxy, http proxy, anonymous email & VPN!  Surf the Dark Web with AnonyProxies :). <!-- Attribution link to AnonyProxies.com is required! Email founders@anonyproxies.com and we can add your website to our list of mirrors sending you ample traffic! --> <a href='privacy.html'>Privacy policy</a>. <a href='/team.html'>Our team</a>. 
                
                <br/><br/>
                <a href='sponsors.html'>Our Sponsors</a>
                
                <?php include("templates/sponsors.html"); ?>
                
            
            </p>
        </div>
    </footer>
    
    <?php include("templates/modals.html"); ?>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55265328-1', 'auto');
        ga('send', 'pageview');
    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        // This identifies your website in the createToken call below
        Stripe.setPublishableKey('pk_live_NTIgT7A7OTic2PIZz3gEdY0S');
         // ...
    </script>

</body>

</html>
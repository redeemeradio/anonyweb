#!/usr/bin/perl
#        _____ 
# __   _|___ / 
# \ \ / / |_ \ 
#  \ V / ___) |  Page Gen
#   \_/ |____/ 
#
# Version: 0.2 beta
#
# Jeremy Goodwin (nexcellent@hotmail.com)
#
##########################################

use MIME::Base64;
&GetFormInput;
&v3_options;

### prevent v3 looping
if (lc($ENV{'HTTP_USER_AGENT'}) eq lc($options{'user_agent'})) {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}\n\n";
	exit 0;
}

### LE fooling code
if ($ENV{'HTTP_USER_AGENT'} =~/LECodeChecker/ig) {
	print "Content-Type: text/html\n\n";
	print '<html><body bgcolor=#ffffff text=#000000 link=#000099>
<!-- BEGIN LINKEXCHANGE CODE --> 
<center><iframe src="http://leader.linkexchange.com/4/X1511758/showiframe?" width=468 height=60 marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>
<a href="http://leader.linkexchange.com/4/X1511758/clickle" target="_top"><img width=468 height=60 border=0 ismap alt="" src="http://leader.linkexchange.com/4/X1511758/showle?"></a></iframe><br><a href="http://leader.linkexchange.com/4/X1511758/clicklogo" target="_top"><img src="http://leader.linkexchange.com/4/X1511758/showlogo?" width=468 height=16 border=0 ismap alt=""></a><br></center>
<!-- END LINKEXCHANGE CODE -->
<br><br><font size=3 face=arial>Click
<a href="'.$options{'script_base_dir'}.$options{'script_name_v3_page'}.'" target="_top">here</a>
to continue!</font>
</body></html>';
	exit 0;
}


### show error if logs disabled
if (lc($options{'logging'}) ne 'on') {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}?type=p&p=nologs\n\n";
	exit 0;
}


$lit = lc($field{'type'});


### first page
if ($lit eq '') {
	### print page with form
	$form = "<form action=\"$options{'script_base_dir'}$options{'script_name_v3_log'}\" method=post>";
	$peg = `./$options{'script_name_v3_page'} p log_passwd`;
	$peg=~s/#\]form/$form/ig;

	print "Content-Type: text/html\n\n";
	print $peg;

	exit 0;
}


### check passwd
$pass = $field{'password'};
if (substr($pass,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
	$pass = &v3_d64(substr($pass,length($options{'misc_enc'}),length($pass)-length($options{'misc_enc'})));
}
if ($options{'log_passwd'} ne $pass) {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}?type=p&p=log_inv\n\n";
	exit 0;
}


### file printing
if ($lit eq 'file') {
	print "Content-Type: text/html\n\n";
	if (-e "$options{'log_dir'}$field{'l'}") {
		print "<html><head><title>$field{'l'}</title></head><body bgcolor=white text=black><font size=3>\n<code><pre>";
		open(HE,"$options{'log_dir'}$field{'l'}");
			while (<HE>) {
				$f=$_;
				$f=~s/<\/pre>/<#\/pre>/ig;
				print $f;
			}
		close(HE);
		print "</pre></code></font></body></html>";
	} else {print "<html><body bgcolor=white text=black><font size=4 face=arial>File '$field{'l'}' doesn't excist!</font></body></html>";}
	exit 0;
}


print "Content-Type: text/html\n\n";

### do stats
if ($lit eq 's') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$mp=0;
	$mf=0;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$p = "$ud-$um-$year";
		
		$pc = 0;
		$fc = 0;
		if (-e "$options{'log_dir'}$p.page.log") {
			open(HE,"$options{'log_dir'}$p.page.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$pc = $tm;
		} else {$pc=0;}
		if (-e "$options{'log_dir'}$p.file.log") {
			open(HE,"$options{'log_dir'}$p.file.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$fc = $tm;
		} else {$fc=0;}

		if ($pc > $mp) {$mp = $pc;}
		if ($fc > $mf) {$mf = $fc;}
		push @stats, "$p ; $pc ; $fc";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}

	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=230 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Page Requests</b></td>
<td width=230 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>File Requests</b></td>
<td width=60 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Hits</b></td></tr>
';
	foreach $one (@stats) {
		@sep = split (' ; ',$one);
		if ($mp!=0) {$wp = int((170 / $mp) * $sep[1]);
		} else {$wp=0;}
		if ($mf!=0) {$wf = int((170 / $mf) * $sep[2]);
		} else {$wf=0;}
		$th=$sep[1]+$sep[2];
		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		if ($sep[1] ne '0') {$npage="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].page.log\" target=\"_top\" class=ltxt>$sep[1]</a>";
		} else {$npage=$sep[1];}


		if ($sep[2] ne '0') {$nfile="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].file.log\" target=\"_top\" class=ltxt>$sep[2]</a>";
		} else {$nfile=$sep[2];}

		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=230 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=190 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wp height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=40 align=center valign=center class=gotxt>&nbsp;&nbsp;".$npage."&nbsp;&nbsp;</td>
      </tr>
    </table>
  </td>
  <td width=230 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=190 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wf height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=40 align=center valign=center class=gotxt>&nbsp;&nbsp;".$nfile."&nbsp;&nbsp;</td>
      </tr>
    </table>
  </td>
  <td width=60 height=30 align=center valign=center bgcolor=#d8f7ff class=gotxt>&nbsp;&nbsp;".$th."&nbsp;&nbsp;</td>
</tr>";
	}

	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for site requests (HTML Pages & Files)";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}






### do errors
if ($lit eq 'e') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$me=0;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$e = "$ud-$um-$year";
		
		$ec = 0;

		if (-e "$options{'log_dir'}$e.error.log") {
			open(HE,"$options{'log_dir'}$e.error.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
		}
			$ec = $tm;
		} else {$ec=0;}
		
		if ($ec > $me) {$me = $ec;}
		push @stats, "$e ; $ec";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}

	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=520 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Errors</b></td></tr>
';
	foreach $one (@stats) {
		@sep = split (' ; ',$one);
		if ($me!=0) {$wp = int((460 / $me) * $sep[1]);
		} else {$wp=0;}
		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		if ($sep[1] ne '0') {$npage="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].error.log\" target=\"_top\" class=ltxt>$sep[1]</a>";
		} else {$npage=$sep[1];}


		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=520 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=480 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wp height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=40 align=center valign=center class=gotxt>&nbsp;&nbsp;".$npage."&nbsp;&nbsp;</td>
      </tr>
    </table>
  </td>
</tr>";
			}

	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for errors (Request errors)";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}





### do email
if ($lit eq 'm') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$me=0;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$e = "$ud-$um-$year";
		
		$ec = 0;

		if (-e "$options{'log_dir'}$e.email.log") {
			open(HE,"$options{'log_dir'}$e.email.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
		}
			$ec = $tm;
		} else {$ec=0;}
		
		if ($ec > $me) {$me = $ec;}
		push @stats, "$e ; $ec";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}

	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=520 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>E-Mail Uses</b></td></tr>
';
	foreach $one (@stats) {
		@sep = split (' ; ',$one);
		if ($me!=0) {$wp = int((460 / $me) * $sep[1]);
		} else {$wp=0;}
		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		if ($sep[1] ne '0') {$npage="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].email.log\" target=\"_top\" class=ltxt>$sep[1]</a>";
		} else {$npage=$sep[1];}


		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=520 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=480 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wp height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=40 align=center valign=center class=gotxt>&nbsp;&nbsp;".$npage."&nbsp;&nbsp;</td>
      </tr>
    </table>
  </td>
</tr>";
	}

	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for e-mail uses";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}





### do frontend
if ($lit eq 'f') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$me=0;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$e = "$ud-$um-$year";
		
		$ec = 0;

		if (-e "$options{'log_dir'}$e.frontend.log") {
			open(HE,"$options{'log_dir'}$e.frontend.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$ec = $tm;
		} else {$ec=0;}
		
		if ($ec > $me) {$me = $ec;}
		push @stats, "$e ; $ec";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}


	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=520 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Hits For v3.2 Front End</b></td></tr>
';

	foreach $one (@stats) {


		@sep = split (' ; ',$one);
		if ($me!=0) {$wp = int((460 / $me) * $sep[1]);
		} else {$wp=0;}
		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		if ($sep[1] ne '0') {$npage="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].frontend.log\" target=\"_top\" class=ltxt>$sep[1]</a>";
		} else {$npage=$sep[1];}


		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=520 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=480 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wp height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=40 align=center valign=center class=gotxt>&nbsp;&nbsp;".$npage."&nbsp;&nbsp;</td>
      </tr>
    </table>
  </td>
</tr>";
	}


	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for requests for v3.2 frontend pages (Jumpstation etc)";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}







### do all
if ($lit eq 'a') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$p = "$ud-$um-$year";
		

		if (-e "$options{'log_dir'}$p.page.log") {
			open(HE,"$options{'log_dir'}$p.page.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$pc = $tm;
		} else {$pc=0;}
		if (-e "$options{'log_dir'}$p.file.log") {
			open(HE,"$options{'log_dir'}$p.file.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$fc = $tm;
		} else {$fc=0;}
		if (-e "$options{'log_dir'}$p.error.log") {
			open(HE,"$options{'log_dir'}$p.error.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$ec = $tm;
		} else {$ec=0;}
		if (-e "$options{'log_dir'}$p.frontend.log") {
			open(HE,"$options{'log_dir'}$p.frontend.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$vc = $tm;
		} else {$vc=0;}
		if (-e "$options{'log_dir'}$p.email.log") {
			open(HE,"$options{'log_dir'}$p.email.log");
				@foot=<HE>;
			close(HE);
			$tm=0;
			foreach $line (@foot) {
				if ((substr($line,0,1) ne '#') and ($line ne '')) {
					$tm++;
				}
			}
			$mc = $tm;
		} else {$mc=0;}

		push @stats, "$p ; $pc ; $fc ; $ec ; $vc ; $mc";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}

	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=74 height=35 align=center valign=center class=gotxt bgcolor=#bbddff><b>R:Pages</b></td>
<td width=74 height=35 align=center valign=center class=gotxt bgcolor=#bbddff><b>R:Files</b></td>
<td width=74 height=35 align=center valign=center class=gotxt bgcolor=#bbddff><b>R:Hits</b></td>
<td width=74 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Errors</b></td>
<td width=74 height=35 align=center valign=center class=gotxt bgcolor=#bbddff><b>FrontEnd</b></td>
<td width=74 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>E-Mails</b></td>
<td width=76 height=35 align=center valign=center class=gotxt bgcolor=#bbddff><b>Total</b></td>
</tr>
';
	foreach $one (@stats) {
		@sep = split (' ; ',$one);

		$th=$sep[1]+$sep[2];
		$ct=$sep[1]+$sep[2]+$sep[3]+$sep[4]+$sep[5];

		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		if ($sep[1] ne '0') {$npage="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].page.log\" target=\"_top\" class=ltxt>$sep[1]</a>";
		} else {$npage=$sep[1];}


		if ($sep[2] ne '0') {$nfile="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].file.log\" target=\"_top\" class=ltxt>$sep[2]</a>";
		} else {$nfile=$sep[2];}

		if ($sep[3] ne '0') {$nerr="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].error.log\" target=\"_top\" class=ltxt>$sep[3]</a>";
		} else {$nerr=$sep[3];}

		if ($sep[4] ne '0') {$nfront="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].frontend.log\" target=\"_top\" class=ltxt>$sep[4]</a>";
		} else {$nfront=$sep[4];}

		if ($sep[5] ne '0') {$nemail="<a href=\"$options{'script_base_dir'}$options{'script_name_v3_log'}?type=file&password=$options{'misc_enc'}".&v3_e64($pass)."&l=$sep[0].email.log\" target=\"_top\" class=ltxt>$sep[5]</a>";
		} else {$nemail=$sep[5];}

		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=74 height=30 align=center valign=center bgcolor=#c8e7ff class=gotxt>
    &nbsp;&nbsp;".$npage."&nbsp;&nbsp;
  </td>
  <td width=74 height=30 align=center valign=center bgcolor=#c8e7ff class=gotxt>
    &nbsp;&nbsp;".$nfile."&nbsp;&nbsp;
  </td>
  <td width=74 height=30 align=center valign=center bgcolor=#c8e7ff class=gotxt>
    &nbsp;&nbsp;(".$th.")&nbsp;&nbsp;
  </td>
  <td width=74 height=30 align=center valign=center bgcolor=#d8f7ff class=gotxt>
    &nbsp;&nbsp;".$nerr."&nbsp;&nbsp;
  </td>
  <td width=74 height=30 align=center valign=center bgcolor=#c8e7ff class=gotxt>
    &nbsp;&nbsp;".$nfront."&nbsp;&nbsp;
  </td>
  <td width=74 height=30 align=center valign=center bgcolor=#d8f7ff class=gotxt>
    &nbsp;&nbsp;".$nemail."&nbsp;&nbsp;
  </td>
  <td width=76 height=30 align=center valign=center bgcolor=#c8e7ff class=gotxt>
    &nbsp;&nbsp;".$ct."&nbsp;&nbsp;</td>
  </td>
</tr>";
	}

	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for all requests ([HTML Pages & Files] [Errors] [Front End] [E-Mails])";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}





### do dataflow
if ($lit eq 'd') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$mi=0;
	$mo=0;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$p = "$ud-$um-$year";
		
		$inc = 0;
		$outc = 0;
		if (-e "$options{'log_dir'}$p.data.log") {
			open(HE,"$options{'log_dir'}$p.data.log");
				@foot=<HE>;
			close(HE);
			foreach $line (@foot) {
				($sia,$sib) = split(' : ',$line,2);
				if (lc($sia) eq 'in') {$inc=$sib;}
				if (lc($sia) eq 'out') {$outc=$sib;}
			}
		} else {
			$inc=0;
			$outc=0;
		}

		if ($outc > $mo) {$mo = $outc;}
		if ($inc > $mi) {$mi = $inc;}
		push @stats, "$p ; $inc ; $outc";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}

	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=230 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Input Data</b></td>
<td width=230 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Output Data</b></td>
<td width=60 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Total</b></td></tr>
';
	foreach $one (@stats) {
		@sep = split (' ; ',$one);
		if ($mi!=0) {$wi = int((120 / $mi) * $sep[1]);
		} else {$wi=0;}
		if ($mo!=0) {$wo = int((120 / $mo) * $sep[2]);
		} else {$wo=0;}
		$th=$sep[1]+$sep[2];
		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=230 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=140 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wi height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=90 align=center valign=center class=gotxt>&nbsp;&nbsp;".&nicesize($sep[1])."</td>
      </tr>
    </table>
  </td>
  <td width=230 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=140 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wo height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=90 align=center valign=center class=gotxt>&nbsp;&nbsp;".&nicesize($sep[2])."</td>
      </tr>
    </table>
  </td>
  <td width=60 height=30 align=center valign=center bgcolor=#d8f7ff class=gotxt>&nbsp;&nbsp;".&nicesize($th)."&nbsp;&nbsp;</td>
</tr>";
	}

	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for data flow (Input, Output & Total)";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}





### do dataflow
if ($lit eq 'b') {

	$dub = `date +%d-%m-%Y`;
	chomp ($dub);
	
	$maxres = $field{'max'};
	
	@va = split ('-',$dub);
	$day = $va[0];
	$month = $va[1];
	$year = $va[2];
	
	my @stats;
	$mi=0;
	$cnt = 0;
	
	for ($z=0;$z<1;$z--) {
	
		$z=0;
		$cnt++;
		if (($maxres != -1) and ($cnt > $maxres)) {last;}
	
		if (length($day)==1) {$ud="0$day";
		} else {$ud=$day;}
		if (length($month)==1) {$um="0$month";
		} else {$um=$month;}
		$p = "$ud-$um-$year";
		
		$inc = 0;
		if (-e "$options{'log_dir'}$p.ads.log") {
			open(HE,"$options{'log_dir'}$p.ads.log");
				@foot=<HE>;
			close(HE);
			foreach $line (@foot) {
				($sia,$sib) = split(' : ',$line,2);
				if (lc($sia) eq 'ads') {$inc=$sib;}
			}
		} else {
			$inc=0;
		}

		if ($inc > $mi) {$mi = $inc;}
		push @stats, "$p ; $inc";

		&cycledate;
	
		if ($year < 2001) {last;}
		
	}

	$out = "<span class=gotxt>Days shown: <b>$maxres</b></span><br><br>";
	$out =$out. '<table border=1 cellpadding=4 cellspacing=2 width=600>
<tr><td width=80 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Date</b></td>
<td width=520 height=35 align=center valign=center class=gotxt bgcolor=#cceeff><b>Banner Ad Impressions</b></td></tr>
';

	foreach $one (@stats) {


		@sep = split (' ; ',$one);
		if ($mi!=0) {$wp = int((460 / $mi) * $sep[1]);
		} else {$wp=0;}
		if ($sep[0] eq $dub) {$dd = 'Today';
		} else {$dd = $sep[0];}

		
		$out=$out. "
<tr>
  <td width=80 height=30 align=center valign=center class=gotxt bgcolor=#d8f7ff>$dd</td>
  <td width=520 height=30 align=center valign=center bgcolor=#d8f7ff>
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=30>
      <tr>
        <td width=480 align=left valign=center>
          <table border=0 cellpadding=0 cellspacing=0 width=$wp height=20>
            <tr>
              <td width=100% bgcolor=#4488ee>&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width=40 align=center valign=center class=gotxt>".$sep[1]."</td>
      </tr>
    </table>
  </td>
</tr>";
	}


	$out=$out. "</tr></table><br>\n";

	$lt="Statistics for total banner ad impressions";

	$peg = `./$options{'script_name_v3_page'} p log`;
	$peg =~s/#\]type/$lt/ig;
	$peg =~s/#\]contents/$out/ig;
	print $peg;

	exit 0;
}








exit 0;

#########################################

sub v3_options {
	open(SF,"v3-config/options");
	  @opf=<SF>;
	close(SF);
	foreach $opd (@opf) {
		$opd=~s/\r/ /ig;
		$opd=~s/\t/ /ig;
		$opn='';
		$opc='';
		$ops=0;
		for ($opz=0;$opz<length($opd);$opz++) {
			$ope=substr($opd,$opz,1);
			if ($ope eq '#') {last;}
			if (($ops==0) and ($ope ne ' ')) {
				$opn=$ope;
				$ops=1;
				next;
			}
			if ($ops==1) {
				if ($ope eq ' ') {$ops=2;
				} else {$opn=$opn.$ope;}
				next;
			}
			if ($ops==2) {
				if (($ope ne ' ') and ($ope ne '=')) {
					$opc=$ope;
					$ops=3;
					next;
				}
			}
			if ($ops==3) {
				if ($ope eq ';') {
					$ops=4;
					last;
				} else {$opc=$opc.$ope;}
			}
		}
		$opn=lc($opn);
		if ($ops==4) {
			$options{$opn}=$opc;
		}
	}

}

###########

sub GetFormInput {

	(*fval) = @_ if @_ ;

	local ($buf);
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN,$buf,$ENV{'CONTENT_LENGTH'});
	}
	else {
		$buf=$ENV{'QUERY_STRING'};
	}
	if ($buf eq "") {
			return 0 ;
		}
	else {
 		@fval=split(/&/,$buf);
		foreach $i (0 .. $#fval){
			($name,$val)=split (/=/,$fval[$i],2);
			$val=~tr/+/ /;
			$val=~ s/%(..)/pack("c",hex($1))/ge;
			$name=~tr/+/ /;
			$name=~ s/%(..)/pack("c",hex($1))/ge;

			if (!defined($field{$name})) {
				$field{$name}=$val;
			}
			else {
				$field{$name} .= ",$val";
				
			
			}


		   }
		}
return 1;
}

###########

sub cycledate {
	$day = $day-1;
	if ($day==0) {
		$month = $month-1;
		if ($month==0) {
			$year = $year-1;
			$month = 12;
		}
		if ($month==1) {$day=31;}
		if ($month==2) {
			$leap = 0;
			$la = $year / 400;
			if ($la == int($la)) {
			        $leap = 1;
			} else {
			        $lb = $year / 100;
			        if ($lb == int($lb)) {
			                $leap = 0;
			        } else {
			                $lc = $year / 4;
			                if ($lc == int($lc)) {
			                        $leap = 1;
			                } else {
			                        $leap = 0;
			                }
			        }
			}
			if ($leap==1) {$day=29;
			} else {$day=28;}
		}
		if ($month==3) {$day=31;}
		if ($month==4) {$day=30;}
		if ($month==5) {$day=31;}
		if ($month==6) {$day=30;}
		if ($month==7) {$day=31;}
		if ($month==8) {$day=31;}
		if ($month==9) {$day=30;}
		if ($month==10) {$day=31;}
		if ($month==11) {$day=30;}
		if ($month==12) {$day=31;}
	}

}

###########

sub v3_e64 {
	$e64=encode_base64($_[0]);
	$e64=~s/=/-/ig;
	$e64=~s/\n/:/ig;
	return $e64;
}

###########

sub v3_d64 {
	$e64=$_[0];
	$e64=~s/-/=/ig;
	$e64=~s/:/\n/ig;
	return decode_base64($e64);
}

###########

sub stringsplit {
	$str=$_[0];
	$mxc=$_[1];
	$out='';
	$lop=0;
	for ($g=0;$g<length($str);$g++) {
		$out = $out . substr($str,$g,1);
		if ($lop==$mxc) {
			$out = $out . '<br>\n';
			$lop = 0;
		}
		$lop++;
	}
	return $out;
}

###########

sub nicesize {
	if ($_[0] <= 0) {return $_[0];}
	if (($_[0] / 1024) < 1) {
		return '<b>'.dp_num($_[0]).'</b>&nbsp;B';
	} elsif (($_[0] / 1048576) < 1) {
		return '<b>'.dp_num($_[0] / 1024).'</b>&nbsp;KB';
	} else {
		return '<b>'.dp_num($_[0] / 1048576).'</b>&nbsp;MB';
	}

}

###########

sub dp_num {

	$ndb='';
	$nda='';
	($nda,$ndb) = split(/\./,$_[0],2);
	if ($ndb eq '') {return $nda;}
	if (length($ndb)>=2) {return $nda.'.'.substr($ndb,0,2);}
	if (length($nbb)==1) {return $nda.'.'.$ndb.'0';}
	return $nda;

}

###########
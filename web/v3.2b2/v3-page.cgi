#!/usr/bin/perl
#        _____ 
# __   _|___ / 
# \ \ / / |_ \ 
#  \ V / ___) |  Page Gen
#   \_/ |____/ 
#
# Version: 0.2 beta2
#
# Jeremy Goodwin (nexcellent@hotmail.com)
#
##########################################

use MIME::Base64;

use CGI;
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);

&GetFormInput;
&v3_options;


### prevent v3 looping
if ((lc($ENV{'HTTP_USER_AGENT'}) eq lc($options{'user_agent'})) or (lc($ENV{'HTTP_USER_AGENT'}) eq lc($options{'ua_old'}))) {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}\n\n";
	exit 0;
}

### LE fooling code
if ($ENV{'HTTP_USER_AGENT'} =~/LECodeChecker/ig) {
	print "Content-Type: text/html\n\n";
	print '<html><body bgcolor=#ffffff text=#000000 link=#000099>
<!-- BEGIN LINKEXCHANGE CODE --> 
<center><iframe src="http://leader.linkexchange.com/4/X1511758/showiframe?" width=468 height=60 marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>
<a href="http://leader.linkexchange.com/4/X1511758/clickle" target="_top"><img width=468 height=60 border=0 ismap alt="" src="http://leader.linkexchange.com/4/X1511758/showle?"></a></iframe><br><a href="http://leader.linkexchange.com/4/X1511758/clicklogo" target="_top"><img src="http://leader.linkexchange.com/4/X1511758/showlogo?" width=468 height=16 border=0 ismap alt=""></a><br></center>
<!-- END LINKEXCHANGE CODE -->
<br><br><font size=3 face=arial>Click
<a href="'.$options{'script_base_dir'}.$options{'script_name_v3_page'}.'" target="_top">here</a>
to continue!</font>
</body></html>';
	exit 0;
}


### redirect to log page if asked to
if ($field{'url'} eq 'v3-logs') {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_log'}\n\n";
	exit 0;
}


### check if http or file request
if ($ARGV[0] eq '') {
	$act=$field{'type'};
	$cak=0;
} else {
	$act=$ARGV[0];
	$cak=1;
}



### set vars
$loc_v3r = $options{'script_base_dir'}.$options{'script_name_v3_r'};
$loc_v3page = $options{'script_base_dir'}.$options{'script_name_v3_page'};
$loc_v3email = $options{'script_base_dir'}.$options{'script_name_v3_email'};
$rep_email = $loc_v3page."?type=email";
$rep_help = $loc_v3page."?type=p&p=help";
$rep_tos = $loc_v3page."?type=p&p=tos";
$rep_adv = $loc_v3page."?type=p&p=advanced";
$rep_faq = $loc_v3page."?type=p&p=faq";
$trandot = $options{'trandot_loc'};



### count total lines in all files
if (($act eq 'mp') and ($field{'url'} eq 'v3-lines')) {
	my @files;
	push @files , $options{'local_base'}.$options{'script_name_v3_r'};
	push @files , $options{'local_base'}.$options{'script_name_v3_page'};
	push @files , $options{'local_base'}.$options{'script_name_v3_email'};
	push @files , $options{'local_base'}.$options{'script_name_v3_log'};
	push @files , $options{'local_base'}.'v3-config/options';
	push @files , $options{'local_base'}.'v3-config/quotes.gen';
	push @files , $options{'local_base'}.'v3-config/quotes.v3';
	push @files , $options{'local_base'}.'v3-pages/censor.jsr';
	push @files , $options{'local_base'}.'v3-pages/write.jsr';
	push @files , $options{'local_base'}.'v3-pages/v3.ad';
	push @files , $options{'local_base'}.'v3-pages/v3.top';
	push @files , $options{'local_base'}.'v3-pages/v3.head';
	push @files , $options{'local_base'}.'v3-pages/v3.foot';
	push @files , $options{'local_base'}.'v3-pages/advanced.fil';
	push @files , $options{'local_base'}.'v3-pages/auth.fil';
	push @files , $options{'local_base'}.'v3-pages/email.fil';
	push @files , $options{'local_base'}.'v3-pages/email_error.fil';
	push @files , $options{'local_base'}.'v3-pages/email_sent.fil';
	push @files , $options{'local_base'}.'v3-pages/err.fil';
	push @files , $options{'local_base'}.'v3-pages/faq.fil';
	push @files , $options{'local_base'}.'v3-pages/file.fil';
	push @files , $options{'local_base'}.'v3-pages/help.fil';
	push @files , $options{'local_base'}.'v3-pages/log.fil';
	push @files , $options{'local_base'}.'v3-pages/log_inv.fil';
	push @files , $options{'local_base'}.'v3-pages/log_passwd.fil';
	push @files , $options{'local_base'}.'v3-pages/main.fil';
	push @files , $options{'local_base'}.'v3-pages/no_javascript.fil';
	push @files , $options{'local_base'}.'v3-pages/noemail.fil';
	push @files , $options{'local_base'}.'v3-pages/nologs.fil';
	push @files , $options{'local_base'}.'v3-pages/tos.fil';
	push @files , $options{'local_base'}.'v3-pages/404.err';
	push @files , $options{'local_base'}.'v3-pages/err.err';
	push @files , $options{'local_base'}.'v3-pages/err_status.err';
	push @files , $options{'local_base'}.'v3-pages/inv_head.err';
	push @files , $options{'local_base'}.'v3-pages/inv_host.err';
	push @files , $options{'local_base'}.'v3-pages/inv_ptc.err';
	push @files , $options{'local_base'}.'v3-pages/inv_req.err';
	push @files , $options{'local_base'}.'v3-pages/max_loc.err';
	push @files , $options{'local_base'}.'v3-pages/no_tcp.err';
	push @files , $options{'local_base'}.'v3-pages/prx_400.err';
	push @files , $options{'local_base'}.'v3-pages/prx_no_tcp.err';

	print "Content-Type: text/plain\n\n";
	print "Total Line Count:\n-----------------\n";
	$jez = 0;
	foreach $file (@files) {
		if (-e $file) {
			$jaz = 0;
			open(HE, $file);
				while (<HE>) {$jaz++;}
			close(HE);
			$jez = $jez + $jaz;
			print "$file = $jaz\n";
		}
	}
	print "-----------------\nTotal: $jez";
	exit 0;
}


### show script source in html
if (($act eq 'mp') and ($field{'url'} eq 'v3-source')) {
	$cup=0;
	print "Content-Type: text/html\n\n";
	print "<html><body bgcolor=#ffffff text=#000000><fixed><font face=arial size=4>\n";
	print "v3.2: Script source for request, page, email and log v3 scripts.<br>\n";
	print "----------------------------------------------------------------</font><br><br><br>\n";
	print "<font face=courier size=3><b>REQUEST SCRIPT: ($options{'script_name_v3_r'})<br>\n--------------</b><br><br></font><font size=2 face=courier,fixed>\n";
	&p_ss($options{'local_base'}.$options{'script_name_v3_r'});
	print "</font><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\n";
	print "<font face=courier,fixed size=3><b>PAGE GEN SCRIPT: ($options{'script_name_v3_page'})<br>\n--------------</b><br><br></font><font size=2 face=courier,fixed>\n";
	&p_ss($options{'local_base'}.$options{'script_name_v3_page'});
	print "</font><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\n";
	print "<font face=courier,fixed size=3><b>EMAIL SCRIPT: ($options{'script_name_v3_email'})<br>\n--------------</b><br><br></font><font size=2 face=courier,fixed>\n";
	&p_ss($options{'local_base'}.$options{'script_name_v3_email'});
	print "</font><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\n";
	print "<font face=courier,fixed size=3><b>LOG SCRIPT: ($options{'script_name_v3_log'})<br>\n--------------</b><br><br></font><font size=2 face=courier,fixed>\n";
	&p_ss($options{'local_base'}.$options{'script_name_v3_log'});
	print "</font><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\n";
	print "<font size=3 face=arial><b>Line Count: (in request, page, email & log scripts) Total: $cup</b></font>";
	print "</body></html>";

	exit 0;
}

### show script source
if (($act eq 'mp') and ($field{'url'} eq 'v3-source-text')) {
	$cup=0;
	print "Content-Type: text/plain\n\n";
	print "v3.2: Script source for request, page, email and log v3 scripts.\n";
	print "----------------------------------------------------------------\n\n\n";
	print "REQUEST SCRIPT: ($options{'script_name_v3_r'})\n--------------\n\n";
	&p_sst($options{'local_base'}.$options{'script_name_v3_r'});
	print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	print "PAGE GEN SCRIPT: ($options{'script_name_v3_page'})\n---------------\n\n";
	&p_sst($options{'local_base'}.$options{'script_name_v3_page'});
	print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	print "EMAIL SCRIPT: ($options{'script_name_v3_email'})\n------------\n\n";
	&p_sst($options{'local_base'}.$options{'script_name_v3_email'});
	print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	print "LOG SCRIPT: ($options{'script_name_v3_log'})\n----------\n\n";
	&p_sst($options{'local_base'}.$options{'script_name_v3_log'});
	print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	print "Line Count: (in request, page, email & log scripts) Total: $cup";

	exit 0;
}

### show options and quotes
if (($act eq 'mp') and ($field{'url'} eq 'v3-oq')) {
	print "Content-Type: text/plain\n\n";
	print "v3.2: Contents of options file and both quote files.\n";
	print "----------------------------------------------------------------\n\n\n";
	print "OPTIONS:\n-------\n\n";
	&p_sso($options{'local_base'}.'v3-config/options');
	print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	print "QUOTES (v3)\n-----------\n\n";
	&p_sst($options{'local_base'}.'v3-config/quotes.v3');
	print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	print "QUOTES (general)\n----------------\n\n";
	&p_sst($options{'local_base'}.'v3-config/quotes.gen');

	exit 0;
}




### if MP then do
if ($act eq 'mp') {

	if (substr($field{'url'},0,6) eq 'debug:') {
		$url=$options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=get&'.$options{'string_r'}.$options{'v3rs_var'}.'='.$options{'misc_enc'}.&v3_e64(substr($field{'url'},6,length($field{'url'})-6)).'&'.$options{'string_r'}.'debug=on';
	} else {
		$url=$options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=get&'.$options{'string_r'}.$options{'v3rs_var'}.'='.$options{'misc_enc'}.&v3_e64($field{'url'});
	}	

	if (lc($field{'old'}) eq 'on') {$url=$url.'&'.$options{'string_o'}.$options{'op_ua_old'}.'=on';}
	if (lc($field{'njs'}) eq 'on') {$url=$url.'&'.$options{'string_o'}.$options{'op_njs'}.'=on';}
	if (lc($field{'ea'}) eq 'on') {$url=$url.'&'.$options{'string_o'}.$options{'op_enableapp'}.'=on';}

	if ($field{'aif'} eq 'on') {$url=$url.'&'.$options{'string_o'}.$options{'op_inframe'}.'=on';}


	if (lc($field{'uf'}) ne 'on') {
		print "Location: $url\n\n";
		exit 0;
	} else {

		### do frame
		print "Content-Type: text/html\n\n";
		print "<!-- v3.2 [By: Jeremy Goodwin] -->
<html>
	<head>
		<title>v3</title>
		<frameset rows=\"120,*\" NORESIZE>
			<frame src=\"$loc_v3page?type=tf\" name=\"v3tf\" scrolling=no marginwidth=0 marginheight=0 border=1 NORESIZE>
			<frame src=\"$url&$options{'string_o'}$options{'op_inframe'}=on\" name=\"v3frame\" marginwidth=0 marginheight=0 border=0 NORESIZE>
		</frameset>
	</head>
	<noframes>
	<body bgcolor=white text=black>
		<font size=5 face=verdana>v3</font><br><br>
		<font size=3 face=verdana>Sorry - Your Browser Doesn't Support Frames</font>
	</body>
	</noframes>
</html>";
	exit 0;
	}
}



### if SP then do
if ($act eq 'sp') {

	$url=$options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=get&'.$options{'string_r'}.$options{'v3rs_var'}.'='.$options{'misc_enc'};

	if ($field{'engine'} eq 'av') {$url=$url.&v3_e64('http://www.altavista.com/sites/search/web?kl=XX&q='.$keywords);
	} elsif ($field{'engine'} eq 'yahoo') {$url=$url.&v3_e64('http://search.yahoo.com/bin/search?p='.$keywords);
	} else {$url=$url.&v3_e64('http://www.google.com/search?q='.$keywords);}

	if (lc($field{'uf'}) ne 'on') {
		print "Location: $url\n\n";
		exit 0;
	} else {

		### do frame
		print "Content-Type: text/html\n\n";
		print "<!-- v3.2 [By: Jeremy Goodwin] -->
<html>
	<head>
		<title>v3</title>
		<frameset rows=\"120,*\" NORESIZE>
			<frame src=\"$loc_v3page?type=tf\" name=\"v3tf\" scrolling=no marginwidth=0 marginheight=0 border=1 NORESIZE>
			<frame src=\"$url&$options{'string_o'}$options{'op_inframe'}=on\" name=\"v3frame\" marginwidth=0 marginheight=0 border=0 NORESIZE>
		</frameset>
	</head>
	<noframes>
	<body bgcolor=white text=black>
		<font size=5 face=verdana>v3</font><br><br>
		<font size=3 face=verdana>Sorry - Your Browser Doesn't Support Frames</font>
	</body>
	</noframes>
</html>";
	exit 0;
	}
}



### top frame
if ($act eq 'tf') {
	print "Content-Type: text/html\n\n";
	print "<!-- v3.2 [By: Jeremy Goodwin] -->
<html>
	<head>
		<title>v3</title>
		<frameset cols=\"490,*\" NORESIZE border=0>
			<frame src=\"$loc_v3page?type=ad\" name=\"v3ad\" scrolling=no marginwidth=0 marginheight=0 border=0 NORESIZE>
			<frame src=\"$loc_v3page?type=top\" name=\"v3top\" scrolling=no marginwidth=0 marginheight=0 border=0 NORESIZE>
		</frameset>
	</head>
	<noframes>
	<body bgcolor=white text=black>
		<font size=5 face=verdana>v3</font><br><br>
		<font size=3 face=verdana>Sorry - Your Browser Doesn't Support Frames</font>
	</body>
	</noframes>
</html>
";
	exit 0;
}



### top
if ($act eq 'top') {
	print "Content-Type: text/html\n\n";
	print "<!-- v3.2 [By: Jeremy Goodwin -->\n";
	print &p_file("v3-pages/v3.top");
	exit 0;
}



### ad
if ($act eq 'ad') {
	&v3_banner_plus_one;
	print "Content-Type: text/html\n\n";
	print "<!-- v3.2 [By: Jeremy Goodwin -->\n";
	print &p_file("v3-pages/v3.ad");
	exit 0;
}






###### from this point onwards all pages made are frontend pages

### do some logging
&page_log;


###### do large pages
if (($act eq '') and ($cak==0)) {
	###print "Set-Cookie: $options{'cookie_string'}=; domain=".$options{'cookie_domain'}.'; path='.$options{'cookie_path'}."; expires=Saturday, 31-Mar-1999 08:00:00 GMT\n";
	print "Set-Cookie: $options{'cookie_string'}=\n";
}
if ($cak==0) {print "Content-Type: text/html\n\n";}

### open and print header
print "<!-- v3.2 [By: Jeremy Goodwin -->\n";
&p_file("v3-pages/v3.head");

### page
if (($act eq 'err') or ($act eq 'p') or ($act eq 'email') or ($act eq '')) {
	if (($act eq 'err') or ($act eq 'p')) {
		if ($field{'p'} ne '') {$ed=$field{'p'};
		} else {$ed=$ARGV[1];}
		if ($act eq 'err') {
			if (-e "$options{'local_base'}v3-pages/$ed.err") {
				$usp=$options{'local_base'}."v3-pages/$ed.err";
			} else {
				$usp=$options{'local_base'}."v3-pages/err.err";
			}
		} else {
			if (-e "$options{'local_base'}v3-pages/$ed.fil") {
				$usp=$options{'local_base'}."v3-pages/$ed.fil";
			} else {
				$usp=$options{'local_base'}."v3-pages/err.fil";
			}
		}
	}
	if ($act eq 'email') {
		if (lc($options{'allow_email'}) eq 'on') {
			$usp=$options{'local_base'}.'v3-pages/email.fil';
			$to=$field{'to'};
			if (substr($to,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
				$to = &v3_d64(substr($to,length($options{'misc_enc'}),length($to)-length($options{'misc_enc'})));
			}
			$cc=$field{'cc'};
			if (substr($cc,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
				$cc = &v3_d64(substr($cc,length($options{'misc_enc'}),length($cc)-length($options{'misc_enc'})));
			}
			$subject=$field{'subject'};
			if (substr($subject,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
				$subject = &v3_d64(substr($subject,length($options{'misc_enc'}),length($subject)-length($options{'misc_enc'})));
			}
			$body=$field{'body'};
			if (substr($body,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
				$body = &v3_d64(substr($body,length($options{'misc_enc'}),length($body)-length($options{'misc_enc'})));
			}
			if ($cc ne '') {$to=$to.' '.$cc;}
		} else {$usp='v3-pages/noemail.fil';}
	}
	if ($act eq '') {$usp=$options{'local_base'}.'v3-pages/main.fil';}
	if ($usp ne '') {
		&p_file($usp);
	}
}

### open and print footer
&p_file('v3-pages/v3.foot');


exit 0;


#########################################

sub v3_options {
	open(SF,"v3-config/options");
	  @opf=<SF>;
	close(SF);
	foreach $opd (@opf) {
		$opd=~s/\r/ /ig;
		$opd=~s/\t/ /ig;
		$opn='';
		$opc='';
		$ops=0;
		for ($opz=0;$opz<length($opd);$opz++) {
			$ope=substr($opd,$opz,1);
			if ($ope eq '#') {last;}
			if (($ops==0) and ($ope ne ' ')) {
				$opn=$ope;
				$ops=1;
				next;
			}
			if ($ops==1) {
				if ($ope eq ' ') {$ops=2;
				} else {$opn=$opn.$ope;}
				next;
			}
			if ($ops==2) {
				if (($ope ne ' ') and ($ope ne '=')) {
					$opc=$ope;
					$ops=3;
					next;
				}
			}
			if ($ops==3) {
				if ($ope eq ';') {
					$ops=4;
					last;
				} else {$opc=$opc.$ope;}
			}
		}
		$opn=lc($opn);
		if ($ops==4) {
			$options{$opn}=$opc;
		}
	}
}

###########

sub GetFormInput {

	(*fval) = @_ if @_ ;

	local ($buf);
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN,$buf,$ENV{'CONTENT_LENGTH'});
	}
	else {
		$buf=$ENV{'QUERY_STRING'};
	}
	if ($buf eq "") {
			return 0 ;
		}
	else {
		$lbuf=$buf;
 		@fval=split(/&/,$buf);
		foreach $i (0 .. $#fval){
			($name,$val)=split (/=/,$fval[$i],2);
			if ($name eq 'keywords') {$keywords=$val;}
			$val=~tr/+/ /;
			$val=~ s/%(..)/pack("c",hex($1))/ge;
			$name=~tr/+/ /;
			$name=~ s/%(..)/pack("c",hex($1))/ge;

			if (!defined($field{$name})) {
				$field{$name}=$val;
			}
			else {
				$field{$name} .= ",$val";
				
			
			}


		   }
		}
return 1;
}

###########

sub v3_e64 {
	$e64=encode_base64($_[0]);
	$e64=~s/=/-/ig;
	$e64=~s/\n/:/ig;
	return $e64;
}

###########

sub v3_d64 {
	$e64=$_[0];
	$e64=~s/-/=/ig;
	$e64=~s/:/\n/ig;
	return decode_base64($e64);
}

##########

sub p_line {

	$f=$_[0];
	$f=~s/##main/$loc_v3page/ig;
	$f=~s/##email/$rep_email/ig;
	$f=~s/##help/$rep_help/ig;
	$f=~s/##tos/$rep_tos/ig;
	$f=~s/##advanced/$rep_adv/ig;
	$f=~s/##faq/$rep_faq/ig;
	$f=~s/#\!r/$loc_v3page/ig;
        $f=~s/#\!email/$loc_v3email/ig;
	$f=~s/#\@t/$trandot/ig;

	$f=~s/#\$to/$to/ig;
	$f=~s/#\$subject/$subject/ig;
	$f=~s/#\$body/$body/ig;

	if ($f=~/#\?v3/i) {
		open(QO,"v3-config/quotes.v3");
			@qof=<QO>;
		close(QO);
		my @quotes;
		foreach $line (@qof) {
			chomp ($line);
			if (($line ne '') and (substr($line,0,1) ne '#')) {
				push @quotes , $line;
			}
		}
		$num = int(rand(scalar(@quotes)));
		$qo = $quotes[$num];
		$f=~s/#\?v3/$qo/ig;
	}

	if ($f=~/#\?quote/i) {
		open(QO,"v3-config/quotes.gen");
			@qof=<QO>;
		close(QO);
		my @quotes;
		foreach $line (@qof) {
			chomp ($line);
			if (($line ne '') and (substr($line,0,1) ne '#')) {
				push @quotes , $line;
			}
		}
		$num = int(rand(scalar(@quotes)));
		$qo = $quotes[$num];
		$f=~s/#\?quote/$qo/ig;
	}

	return $f;
}

##########

sub p_file {
	if (-e $_[0]) {
	
		open(HE,$_[0]);
			while (<HE>) {
				print &p_line($_);
			}
		close(HE);
	}
}

##########

sub p_ss {
	if (-e $_[0]) {
		open(HE,$_[0]);
			while (<HE>) {
			  $ln=$_;
			  $ln=~s/>/&gt;/ig;
			  $ln=~s/</&lt;/ig;
			  $ln=~s/ /&nbsp;/ig;
			  $ln=~s/\t/&nbsp;&nbsp;&nbsp;/ig;
			  print $ln.'<br>';
			  $cup++;
			}
		close(HE);
	}
}

##########

sub p_sst {
	if (-e $_[0]) {
		open(HE,$_[0]);
			while (<HE>) {
			  $ln=$_;
			  print $ln;
			  $cup++;
			}
		close(HE);
	}
}

##########


sub p_sso {
	if (-e $_[0]) {
		open(HE,$_[0]);
			while (<HE>) {
			  $ln=$_;
			  if ($ln=~/log_passwd/) {print "  ### PASSWORD REMOVED FOR PROTECTION ###\n";
			  } else {print $ln;}
			}
		close(HE);
	}
}

##########

sub page_log {

	if ((lc($options{'logging'}) eq 'on') and ($cak==0)) {

		$ll=$lbuf;
		$ll=~s/;/:/ig;
		$ll=~s/\n/ /ig;
		$ll=~s/\r/ /ig;

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);
		$tim = `date +%T`;
		chomp ($tim);

		$logline = "$tim ;>> Vars;: $ll ; User_IP;: $ENV{'REMOTE_ADDR'} ; User_Host;: $ENV{'REMOTE_HOST'} ; User_UA;: $ENV{'HTTP_USER_AGENT'}";
		$logline=~s/\n/\\n/ig;
		$logline=~s/\r//ig;
		$logline=~s/<\/pre>/<\\\/pre>/ig;

		if (-e "$options{'log_dir'}$dub.frontend.log") {

			open (LOG,">>$options{'log_dir'}$dub.frontend.log");
				print LOG "$logline\n";
			close (LOG);

		} else {

			$fulldate = `date +%A`;
			chomp ($fulldate);
			$fulldate = $fulldate . ', ' . `date +%d-%m-%Y`;
			chomp ($fulldate);
			open (LOG,">$options{'log_dir'}$dub.frontend.log");
				print LOG "# v3-log (frontend) > $fulldate\n";
				print LOG "$logline\n";
			close (LOG);
			chmod 0666,"$options{'log_dir'}$dub.frontend.log";

		}

	}

	&v3_banner_plus_one;

}

###########

sub v3_banner_plus_one {

	if (lc($options{'logging'}) eq 'on') {

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);

		$write_bc=0;
		$ed_in=0;
		if (-e "$options{'log_dir'}$dub.ads.log") {

			open (LOG,"$options{'log_dir'}$dub.ads.log");
				@tails = <LOG>;
			close (LOG);
			foreach $tail (@tails) {
				($stn,$std) = split(' : ',$tail,2);
				if (lc($stn) eq 'ads') {$ed_in=$std;}
			}

		}
		$write_bc=$ed_in+1; ## NUMBER OF ADS SHOWN

		open (LOG,">$options{'log_dir'}$dub.ads.log");
			print LOG "ADS : $write_bc\n";
		close (LOG);
		chmod 0666,"$options{'log_dir'}$dub.ads.log";

	}

}

###########
#!/usr/bin/perl
#        _____ 
# __   _|___ / 
# \ \ / / |_ \ 
#  \ V / ___) |  Request Script
#   \_/ |____/ 
#
# Version: 0.3 beta
#
# Jeremy Goodwin (nexcellent@hotmail.com)
#
##########################################
#
# %LINK - look for href and change to a v3 link (checking for what the href contain (^# ^ftp:// etc..))
# %URL - look for specified flag and change to a v3 url
# %AUR - look for specified flag and change to actual http:// path
#

my @process;

push @process , 'a:;%LINK';
push @process , 'area:;%LINK';
push @process , 'img:;%URL:;src:;file';
push @process , 'img:;%URL:;lowsrc:;file';
push @process , 'frame:;%URL:;src:;get';
push @process , 'iframe:;%URL:;src:;get';
push @process , 'td:;%URL:;background:;file';
push @process , 'table:;%URL:;background:;file';
push @process , 'body:;%URL:;background:;file';
push @process , 'link:;%URL:;href:;file';
push @process , 'layer:;%URL:;src:;file';
push @process , 'input:;%URL:;src:;file:;type=image';
push @process , 'applet:;%AUR:;codebase';
push @process , 'embed:;%URL:;src:;file';

##########################################

use IO::Socket;
use Socket;
use MIME::Base64;


use CGI;
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);


&v3_options;
&v3_GetInput;

### prevent v3 looping
if ((lc($ENV{'HTTP_USER_AGENT'}) eq lc($options{'user_agent'})) or (lc($ENV{'HTTP_USER_AGENT'}) eq lc($options{'ua_old'}))) {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}\n\n";
	exit 0;
}

### LE fooling code
if ($ENV{'HTTP_USER_AGENT'} =~/LECodeChecker/ig) {
	print "Content-Type: text/html\n\n";
	print '<html><body bgcolor=#ffffff text=#000000 link=#000099>
<!-- BEGIN LINKEXCHANGE CODE --> 
<center><iframe src="http://leader.linkexchange.com/3/X1511758/showiframe?" width=468 height=60 marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>
<a href="http://leader.linkexchange.com/3/X1511758/clickle" target="_top"><img width=468 height=60 border=0 ismap alt="" src="http://leader.linkexchange.com/3/X1511758/showle?"></a></iframe><br><a href="http://leader.linkexchange.com/3/X1511758/clicklogo" target="_top"><img src="http://leader.linkexchange.com/3/X1511758/showlogo?" width=468 height=16 border=0 ismap alt=""></a><br></center>
<!-- END LINKEXCHANGE CODE -->
<br><br><font size=3 face=arial>Click
<a href="'.$options{'script_base_dir'}.$options{'script_name_v3_page'}.'" target="_top">here</a>
to continue!</font>
</body></html>';
	exit 0;
}

### set misc use vars
$titlei=0;
$imgad=0;
$pux = lc($options{'use_proxy'});
$js_p_c = 0;
$js_p_w = 0;
$inside_js = 0;
$data_in=0;
$data_out=0;
$ads_seen=0;

### check options
if (lc($field_o{$options{'op_njs'}}) eq 'on') {
	push @process , 'script:;%HID';
	push @process , '/script:;%HID';
	push @process , 'noscript:;%HID';
	push @process , '/noscript:;%HID';
	$no_script='on';
}
if (lc($field_o{$options{'op_inframe'}}) eq 'on') {
	$in_frame='on';
}
if (lc($field_o{$options{'op_enableapp'}}) eq 'on') {
	$enable_app='on';
}
if (lc($field_o{$options{'op_ua_old'}}) eq 'on') {
	$ua_old='on';
}

### check debug
#$v3_debug=1;
if ((lc($field_r{'debug'}) eq 'on') or ($v3_debug==1)) {
	$v3_debug=1;
	print "Content-type: text/plain\n\n";
}

&slog("# v3.2 Request Started\n* Processing Inputs\n");

if ($ENV{HTTP_COOKIE} ne '') {
	@ma=split(';',$ENV{HTTP_COOKIE});
	foreach $mb (@ma) {
		@mc=split('=',$mb);
		if (lc($mc[0]) eq $options{'cookie_string'}) {
			$cookies=&v3_d64($mc[1]);
			&v3_checkCookies;
			&slog('% Cookies Found: '.$cookies."\n");
		} else {$cookies='';}
	}
}


### check that request type is valid.. if not return error
if ((lc($field_r{$options{'v3rs_type'}}) ne 'get') and (lc($field_r{$options{'v3rs_type'}}) ne 'formget') and (lc($field_r{$options{'v3rs_type'}}) ne 'formpost') and (lc($field_r{$options{'v3rs_type'}}) ne 'file') and (lc($field_r{$options{'v3rs_type'}}) ne 'jfile') and (lc($field_r{$options{'v3rs_type'}}) ne 'jget')) {
	&v3_loc("inv_req");
	exit 0;
}


### build address using type and var
$http_address=$field_r{$options{'v3rs_var'}};
if (substr($http_address,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
	$http_address=substr($http_address,length($options{'misc_enc'}),length($http_address)-length($options{'misc_enc'}));
	$http_address=v3_d64($http_address);
}

### get javascript address
if ((lc($field_r{$options{'v3rs_type'}}) eq 'jget') or (lc($field_r{$options{'v3rs_type'}}) eq 'jfile')) {
	$aa=0;
	if (lc(substr($exstring,0,6)) eq 'ftp://') {$aa=1;}
	if (lc(substr($exstring,0,9)) eq 'gopher://') {$aa=1;}
	if (lc(substr($exstring,0,8)) eq 'https://') {$aa=1;}
	if (lc(substr($exstring,0,7)) eq 'mailto:') {$aa=1;}
	if (lc(substr($exstring,0,5)) eq 'news:') {$aa=1;}
	if (lc(substr($exstring,0,7)) eq 'telnet:') {$aa=1;}
	if (lc(substr($exstring,0,5)) eq 'wais:') {$aa=1;}
	if (lc(substr($exstring,0,7)) eq 'file://') {$aa=1;}
	if ($aa==1) {
		&v3_loc("inv_ptc");
		exit 0;
	}
	&slog("* Javascript address formed for http:// address\n");
	($ss,$qs_domain,$qs_dir,$ss,$ss)=&v3_getdd($http_address);
	if (lc(substr($exstring,0,7)) eq 'http://') {
		$http_address=$exstring;
		if (substr(lc($http_address),0,length($options{'script_base_dir'})) eq lc($options{'script_base_dir'})) {
			&slog("* Javascript address points to a v3.2 address.\n");
			print "Location: $http_address\n\n";
			exit 0;
		}
	} elsif (substr($exstring,0,1) eq '/') {
		$http_address=substr($qs_domain,0,length($qs_domain)-1).$exstring;
	} else {
		$http_address=$qs_dir.$exstring;
	}
	$exstring='';
	if (lc($field_r{$options{'v3rs_type'}}) eq 'jget') {$field_r{$options{'v3rs_type'}}='get';}
	if (lc($field_r{$options{'v3rs_type'}}) eq 'jfile') {$field_r{$options{'v3rs_type'}}='file';}
}

### working out address
if (lc(substr($http_address,0,7)) ne 'http://') {
	$aa=0;
	if (lc(substr($http_address,0,6)) eq 'ftp://') {$aa=1;}
	if (lc(substr($http_address,0,9)) eq 'gopher://') {$aa=1;}
	if (lc(substr($http_address,0,8)) eq 'https://') {$aa=1;}
	if (lc(substr($http_address,0,7)) eq 'mailto:') {$aa=1;}
	if (lc(substr($http_address,0,5)) eq 'news:') {$aa=1;}
	if (lc(substr($http_address,0,7)) eq 'telnet:') {$aa=1;}
	if (lc(substr($http_address,0,5)) eq 'wais:') {$aa=1;}
	if (lc(substr($http_address,0,7)) eq 'file://') {$aa=1;}
	if ($aa==1) {
		&v3_loc("inv_ptc");
		exit 0;
	} else {
		&slog("* No 'http://' included in address... adding one...\n");
		$http_address = 'http://' . $http_address;
	}
}
if ((lc($field_r{$options{'v3rs_type'}}) eq 'formget') or (lc($field_r{$options{'v3rs_type'}}) eq 'formpost')) {
	$aa='';
	#@ba = keys %field_v;
	#foreach $ab (@ba) {
	#	$aa = $aa.'&'.$ab.'='.$field_v{$ab};
	#}

	for ($ab=0;$ab<$varco;$ab++) {
		$aa = $aa.'&'.$varkn[$ab];
	}

	if (lc($field_r{$options{'v3rs_type'}}) eq 'formget') {
		if ($http_address=~/\?/) {#
		} else {$aa = '?'.substr($aa,1,length($aa)-1);}
		$http_address=$http_address.$aa;
	} else {
		$aa = substr($aa,1,length($aa)-1);
		$post_content=$aa;
	}
}
if (lc($field_r{$options{'v3rs_type'}}) eq 'formpost') {
	$http_method='POST';
} else {
	$http_method='GET';
}

### getting referer
$referer=$field_r{$options{'v3rs_referer'}};
if ($referer ne '') {
	if (substr($referer,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
		$referer=substr($referer,length($options{'misc_enc'}),length($referer)-length($options{'misc_enc'}));
		$referer=v3_d64($referer);
	}
} else {
	if (substr(lc($ENV{'HTTP_REFERER'}),0,length("$options{'script_base_dir'}$options{'script_name_v3_r'}")+1) eq lc("$options{'script_base_dir'}$options{'script_name_v3_r'}?")) {
		$vaq=substr($ENV{'HTTP_REFERER'},length("$options{'script_base_dir'}$options{'script_name_v3_r'}?"),length($ENV{'HTTP_REFERER'})-length("$options{'script_base_dir'}$options{'script_name_v3_r'}?"));
		$vaq=~s/$options{'null_1form'}//ig;
		$vaq=~s/$options{'null_2form'}//ig;
		$vaq=~s/$options{'null_1string'}//ig;
		$vaq=~s/$options{'null_2string'}//ig;
		($vaw,$vax) = split ("$options{'string_r'}$options{'v3rs_ex'}=",$vaq,2);
		@vaq = split(/\&/,$vaw);
		$vrv='';
		$vrt='';
		foreach $vae (@vaq) {
			($vsn,$vsv) = split ('=',$vae,2);
			if ($vsn eq "$options{'string_r'}$options{'v3rs_var'}") {$vrv=$vsv;}
			if ($vsn eq "$options{'string_r'}$options{'v3rs_type'}") {$vrt=$vsv;}
		}
		if (($vrt ne '') and ($vrv ne '')) {
			if (substr($vrv,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
				$vrv=substr($vrv,length($options{'misc_enc'}),length($vrv)-length($options{'misc_enc'}));
				$vrv=v3_d64($vrv);
			}
			if (lc(substr($vrt,0,1)) eq 'j') {
				($ss,$qs_domain,$qs_dir,$ss,$ss)=&v3_getdd($vrv);
				if (lc(substr($vax,0,7)) eq 'http://') {
					$vrv=$vax;
				} elsif (substr($vax,0,1) eq '/') {
					$vrv=substr($qs_domain,0,length($qs_domain)-1).$vax;
				} else {
					$vrv=$qs_dir.$vax;
				}
			} else {
				$vrv=$vrv.$vax;
			}
			$referer = $vrv;
			&slog("* Referer formed from actual referer ('$ENV{'HTTP_REFERER'}').\n");
		}
	}
}


### check for extra string
if (($exstring ne '') and ((lc($field_r{$options{'v3rs_type'}}) eq 'file') or (lc($field_r{$options{'v3rs_type'}}) eq 'get'))) {
	$http_address=$http_address.$exstring;
	&slog("* Extra string found: '$exstring'\n");
}

### validate url
$http_address=&v3_relurl($http_address);

### log a few details
&slog ("[ Referer: $referer\n[ Address: $http_address\n[ URI: $http_uri\n[ Method: $http_method\n");
if ($http_method eq 'POST') {&slog("[ Post Content: $post_content\n");}

### check for auth inputs
if (substr(lc($field_r{$options{'v3rs_auth'}}),0,4) eq 'pos:') {
	$auth_type='POS';
	$ta=substr($field_r{$options{'v3rs_auth'}},4,length($field_r{$options{'v3rs_auth'}})-4);
	$auth_realm=&v3_d64($ta);
	$auth_userid=$field_r{$options{'v3rs_auth_userid'}};
	$auth_passwd=$field_r{$options{'v3rs_auth_passwd'}};
} elsif (substr($field_r{$options{'v3rs_auth'}},0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
	$auth_type='TRY';
	$ta=substr($field_r{$options{'v3rs_auth'}},length($options{'misc_enc'}),length($field_r{$options{'v3rs_auth'}})-length($options{'misc_enc'}));
	$ta=&v3_d64($ta);
	@tb=split(':!:',$ta);
	$auth_realm=$tb[0];
	$auth_userid=$tb[1];
	$auth_passwd=$tb[2];
}

### auth details processing + log
if ($auth_type ne '') {
	if (substr($auth_userid,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
		$auth_userid=substr($auth_userid,length($options{'misc_enc'}),length($auth_userid)-length($options{'misc_enc'}));
		$auth_userid=v3_d64($auth_userid);
	}
	if (substr($auth_passwd,0,length($options{'misc_enc'})) eq $options{'misc_enc'}) {
		$auth_passwd=substr($auth_passwd,length($options{'misc_enc'}),length($auth_passwd)-length($options{'misc_enc'}));
		$auth_passwd=v3_d64($auth_passwd);
	}
	&slog("[ Auth Type: $auth_type\n[ Auth Realm: $auth_realm\n[ Auth UserID: $auth_userid\n[ Auth Passwd: $auth_passwd\n");
}
&slog("\n");

######## this will make everything use lwp
#
# &v3_lwpback;
# exit 0;
#
########

&v3_mainloop;

exit 0;

##############################################################################################

sub v3_GetInput {

	(*fval) = @_ if @_ ;

	my @varkc , @varkn;
	$varco=0;

	$exstring='';
	
	local ($buf);
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN,$buf,$ENV{'CONTENT_LENGTH'});
	}
	else {
		$buf=$ENV{'QUERY_STRING'};
	}
	if ($buf eq "") {
			return 0 ;
		}
	else {
		$exsa=0;
 		@fval=split(/&/,$buf);
		foreach $i (0 .. $#fval){

			if ($exsa==1) {
				$val=$fval[$i];
				$val=~s/$options{'null_1form'}//ig;
				$val=~s/$options{'null_2form'}//ig;
				$val=~s/$options{'null_1string'}//ig;
				$val=~s/$options{'null_2string'}//ig;
				$exstring=$exstring.'&'.$val;
				next;
			}

			($name,$val)=split (/=/,$fval[$i],2);

			if ($name eq "$options{'string_r'}$options{'v3rs_ex'}") {
				$exsa=1;
				$val=~s/$options{'null_1form'}//ig;
				$val=~s/$options{'null_2form'}//ig;
				$val=~s/$options{'null_1string'}//ig;
				$val=~s/$options{'null_2string'}//ig;
				$exstring=$val;
				next;
			}

			$val=~s/$options{'null_1form'}//ig;
			$val=~s/$options{'null_2form'}//ig;
			$name=~s/$options{'null_1form'}//ig;
			$name=~s/$options{'null_2form'}//ig;
			if ((substr($name,0,length($options{'string_r'})) eq $options{'string_r'}) or (substr($name,0,length($options{'string_o'})) eq $options{'string_o'})) {
				$val=~tr/+/ /;
				$val=~ s/%(..)/pack("c",hex($1))/ge;
				$val=~s/$options{'null_1string'}//ig;
				$val=~s/$options{'null_2string'}//ig;
				$name=~tr/+/ /;
				$name=~ s/%(..)/pack("c",hex($1))/ge;
				$name=~s/$options{'null_1string'}//ig;
				$name=~s/$options{'null_2string'}//ig;
			}
			if (substr($name,0,length($options{'string_r'})) eq $options{'string_r'}) {
				$name=substr($name,length($options{'string_r'}),length($name)-length($options{'string_r'}));
				$field_r{$name}=$val;
			} elsif (substr($name,0,length($options{'string_o'})) eq $options{'string_o'}) {
				$name=substr($name,length($options{'string_o'}),length($name)-length($options{'string_o'}));
				$field_o{$name}=$val;
			} else {
				if ($val=~/V3DebuG/) {
					$v3_debug=1;
					$val=~s/V3DebuG//g;
				}
				$field_v{$name}="$val";
				$varkn[$varco]="$name=$val";
				$varco++;
			}

		}
	}
	return 1;
}

###########

sub v3_mainloop {

	### main location loop
	for ($ml=0;$ml<$options{'max_loc'};$ml++) {
	
		### get server info + log
		&slog("* (Location $ml)\n");
		($http_server,$http_domain,$http_dir,$http_address,$http_port)=&v3_getdd($http_address);
		&slog("] Server: $http_server\n] Domain: $http_domain\n] Directory: $http_dir\n] New Address: $http_address\n] Port: $http_port\n");

		### don't use proxy if requesting off localhost
		if ((lc($http_server) eq 'localhost') or ($http_server eq '127.0.0.1')) {$pux='';}

		if ($pux ne 'on') {
		  if ($http_ip=gethostbyname($http_server)) {
		    $http_ip=inet_ntoa($http_ip);
		    &slog("] IP: $http_ip\n");
		  } elsif ($pux eq 'onfail') {
		    	&slog("# Invalid Host. ONFAIL specified, going to try with proxy...\n");
			&v3_lwpback;
			exit 0;
		
		  } else {&v3_loc("inv_host");}
		}
		
		$http_req="/".substr($http_address,length($http_domain),length($http_address)-length($http_domain));
		&slog("] Request: $http_req\n");
		$hh=&v3_gethead;


		### Read Head
		
		### 3xx
		if (substr(&errcode($hh),0,1) eq '3') {
			$mc = &v3_headcon($hh,'Location');
			@mv = split (':!!:',$mc);
			if (scalar(@mv)<1) {
				&slog("* Status 3xx found but no location specified.\n");
				&v3_loc("inv_head");
			}
			@mx = split (' ',$mv[0]);
			$mj=&v3_relurl($mx[0]);
	
			### prevent v3 looping
			if (lc($mj) eq lc("$options{'script_base_dir'}$options{'script_name_v3_page'}")) {
				print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}\n\n";
				exit 0;
			}
	
			if (&v3_SameServer($http_address,$mj)) {
				if ($auth_type ne '') {$auth_type='POS';}
			} else {
				if ($auth_type ne '') {$auth_type='TRY';}
			}
			if ($mj eq $http_address) {$scarb=0;}
			###
			# i worked out that the referer should be the original file... not a 3xx location
			# $referer=$http_address;
			###
			$http_address=$mj;
			if ($scarb==0) {$http_method='GET';}
			&slog ("\n* New Location Specified: \"$http_address\"\n");
			next;
		}

		### 405
		if ((&errcode($hh) eq '405') and ($http_method eq 'POST')) {
			$http_method='GET';
			$ml=$ml-1;
			&slog("\n* Status 405 found and method is on POST. Going to try with GET...\n");
			next;
		}
	
		### 401
		if ((&errcode($hh) eq '401') and ($auth_type eq 'TRY')) {
			$mc = &v3_headcon($hh,'WWW-Authenticate');
			@mv = split (':!!:',$mc);
			if (scalar(@mv)<1) {
				&slog("* Status 401 found but no www-authenticate specified.\n");
				&v3_loc("inv_head");
			}
			$mj=v3_buildauth($mv[0]);
			$mr=v3_searchauth($mj,'realm');
			if ($mr ne $auth_realm) {
				&slog("* Status 401 found. TRY auth realm doesn't match.\n");
				&v3_authpage($mr);
			}
			$auth_type='POS';
			$ml=$ml-1;
			&slog("\n* Status 401 found. TRY auth type available: realm matches.. trying...\n");
			next;
		}
		if ((&errcode($hh) eq '401') and ($auth_type eq 'POS')) {
			$mc = &v3_headcon($hh,'WWW-Authenticate');
			@mv = split (':!!:',$mc);
			if (scalar(@mv)<1) {
				&slog("* Status 401 found but no www-authenticate specified.\n");
				&v3_loc("inv_head");
			}
			$mj=v3_buildauth($mv[0]);
			$mr=v3_searchauth($mj,'realm');
			&slog("* Status 401 recieved. POS auth invalid.\n");
			&v3_invauth($mr);
			exit 0;
		}
		if ((&errcode($hh) eq '401') and ($auth_type eq '')) {
			$mc = &v3_headcon($hh,'WWW-Authenticate');
			@mv = split (':!!:',$mc);
			if (scalar(@mv)<1) {
				&slog("* Status 401 found but no www-authenticate specified.\n");
				&v3_loc("inv_head");
			}
			$mj=v3_buildauth($mv[0]);
			$mr=v3_searchauth($mj,'realm');
			&slog("* Status 401 found. No auth to try..\n");
			&v3_authpage($mr);
			exit 0;
		}
	
		## no conenction
		if ($hh eq '') {
			&slog("# Connection Failed! No connection was actually established.. hmm...\n");
			&v3_lwpback;
			exit 0;
		}
	
		## error not understandable?
		if (&errcode($hh) ne '') {
			&slog("* Invalid status code.\n");
			&v3_loc(&errcode($hh));
		}
	
		last;
	}
	
	if ($ml<$options{'$max_loc'}) {
		&slog("Jumped out of loop.\n");
		&v3_loc("no_tcp");
	} else {
		&v3_loc("max_loc");
	}

}

################

sub v3_getdd {
	$input=$_[0];
	$usedd='';
	if ($http_method eq 'POST') {$usedd='yes';}
	$ni='';
	$cgi='';
	$ja=0;
	for ($z=0;$z<length($input);$z++) {
		if ($ja==0) {
			if (substr($input,$z,1) ne '?') {
				$ni=$ni.substr($input,$z,1);
			} else {
				$ja=1;
				$cgi='?';
				$usedd='yes';
			}
		} else {$cgi = $cgi.substr($input,$z,1);}
	}
	$input=$ni;
        if (substr(lc($input),0,7) ne 'http://') {return false;}
	if (substr($input,length($input)-1,1) eq '/') {
		$t=0;
		for ($z=0;$z<length($input);$z++) {
			if (substr($input,$z,1) eq '/') {$t++;}
		}
		if ($t<=3) {
			$dir=$input;
			$dom=$input;
		} else {
			$dir=$input;
			$r='';
			$rc=0;
			for ($z=0;$z<length($input);$z++) {
				$ra=substr($input,$z,1);
				if ($ra eq '/') {
					$rc++;
					if ($rc<=3) {$r="$r/";}
				}
				if (($ra ne '/') and ($rc<3)) {$r="$r$ra";}
			}
			$dom=$r;
		}
	} else {
		$t=0;
	        for ($z=0;$z<length($input);$z++) {
	                if (substr($input,$z,1) eq '/') {$t++;}
	        }
		if ($t<3) {
			$dom="$input/";
			$dir="$input/";
			} else {
	        	$rq=0;
		        for ($z=length($input)-1;$z>=0;$z--) {
	        		$ra=substr($input,$z,1);   
				if ($ra eq '/') {
					$rq=$z;
					last;
				}
		        }
			$rx=substr($input,0,$rq+1);
			$rz=substr($input,$rq+1,length($input)-$rq-1);
			if (($rz =~/\./) or ($usedd ne '')) {$dir=$rx;} else {$dir="$input/";}
			$r='';      
		        $rc=0;
		        for ($z=0;$z<length($input);$z++) {
		        	$ra=substr($input,$z,1);
		                if ($ra eq '/') {
		                	$rc++;
		                        if ($rc<=3) {$r="$r/";}
		                }
		                if (($ra ne '/') and ($rc<3)) {$r="$r$ra";}
	        	}
			$dom=$r;
		}
	}
	$ser=substr($dom,7,length($dom)-8);
	if (length($dom)>length($input)) {
		$add="$input/";
	} else {
		$add=$input;
	}
	if ($cgi ne '') {$add=$add.$cgi;}
	if ($ser=~/\@/) {
		@ni=split('@',$ser);
		$dom=~s/$ser/$ni[1]/i;
		$dir=~s/$ser/$ni[1]/i;
		$add=~s/$ser/$ni[1]/i;
		$ser=$ni[1];
		if ($auth_type eq '') {
			@ne=split(':',$ni[0]);
			$auth_type='POS';
			$auth_userid=$ne[0];
			$auth_passwd=$ne[1];
			&slog(") address contained auth info...\n) UserID: $auth_userid\n) Passwd: $auth_passwd\n");
		} else {
			&slog(") address conatained auth info - not replaced\n");
		}
	}
	$por=80;
	if ($ser=~/:/) {
		@ni=split(':',$ser);
		$ser=$ni[0];
		$por=$ni[1];
	}
	return ($ser,$dom,$dir,$add,$por);
}

###########

sub slog {
	$trog=$trog.$_[0];
	if ($v3_debug==1) {print $_[0];}
}

###########

sub smog {
	@jog=split("\n",$_[0]);
	if ($_[1] ne '') {$gv=$_[1];
	} else {$gv='<';}
	foreach $dt (@jog) {
		$trog=$trog."$gv $dt\n";
		if ($v3_debug==1) {print "$gv $dt\n";}
	}
}

###########

sub v3_loc {

	$locc = $_[0];
	if (($locc eq 'no_tcp') and (lc($options{'use_proxy'}) eq 'on')) {$locc='prx_no_tcp';}
	if ($locc eq '503') {$locc='inv_host';}

	## log error
	&error_log($locc);

	if (($locc eq '400') and ($pox eq 'on')) {$locc='prx_400';}
	if ((length($locc)==3) and ($locc ne '404')) {
		$fn=$locc;
		$fn=~s/[0-9]//gi;
		if ($fn eq '') {$locc='err_status';}
	}


	if (lc($field_r{$options{'v3rs_type'}}) eq 'file') {

		&slog("\n# Script Exit (v3-error-image)... type=$locc\n");
		print "Location: $options{'error_img_loc'}\n\n";

	} else {

		&slog("\n# Script Exit (v3-page)... type=$locc\n");
	
		$peg = `$options{'local_base'}$options{'script_name_v3_page'} err $locc`;
		$peg=~s/#\]url/$http_address/ig;

		print "Content-Type: text/html\n\n";
		print "<!-- Page Log:\n$trog-->\n";
		print $peg;

	}
	
	exit 0;
}

###########

sub v3_e64 {
	$e64=encode_base64($_[0]);
	$e64=~s/=/-/ig;
	$e64=~s/\n/:/ig;
	return $e64;
}

###########

sub v3_d64 {
	$e64=$_[0];
	$e64=~s/-/=/ig;
	$e64=~s/:/\n/ig;
	return decode_base64($e64);
}

###########

sub v3_gethead {
	$hout='';
	&slog("* Connecting to server...\n");
	my $socket	= gensym;
	my $wordtype	= "";
	my $result = "";
	my $proto	= getprotobyname('tcp');
	$SIG{ALRM} = sub { &slog("# Connection Failed!\n"); &v3_lwpback; exit 0;};
	socket($socket, PF_INET, SOCK_STREAM, $proto) or return false;
	eval {
		alarm 6;
                if ($pux eq 'on') {
		  &slog("* Using proxy server: $options{'proxy_ip'}:$options{'proxy_port'}\n");
		  connect($socket, sockaddr_in($options{'proxy_port'}, inet_aton($options{'proxy_ip'}))) or return false;
		} else {
		  connect($socket, sockaddr_in($http_port, inet_aton($http_ip))) or return false;
                }
		alarm 0;
	};
	if ($@ && $@ ne "alarm\n") {
		&slog("# Connection Failed!\n");
		&v3_lwpback;
	}
	$socket->autoflush(1);
	$seh='';
        if ($pux eq 'on') {
	  $seh=$seh."$http_method $http_address HTTP/1.1\n";
        } else {
	  $seh=$seh."$http_method $http_req HTTP/1.1\n";
	}
	$seh=$seh."Connection: close\n";
	if ($http_port eq '80') {
		$seh=$seh."Host: $http_server\n";
	} else {
		$seh=$seh."Host: $http_server:$http_port\n";
	}
	if ($ua_old eq 'on') {
		$seh=$seh."User-Agent: $options{'ua_old'}\n";
	} else {
		$seh=$seh."User-Agent: $options{'user_agent'}\n";
	}
	if ($referer ne '') {$seh=$seh."Referer: $referer\n";}
	if ($auth_type eq 'POS') {
		$seh=$seh."Authorization: Basic " . encode_base64($auth_userid.":".$auth_passwd);
	}
	$oc=&v3_CookieOut;
	if ($oc ne '') {
		$seh=$seh."Cookie: $oc\n";
	}
	$scarb=0;
	if ($http_method eq 'POST') {
		$scarb=1;
		$ha=length($post_content);
		$seh=$seh."Content-Type: application/x-www-form-urlencoded\n";
		$seh=$seh."Content-Length: $ha\n\n$post_content\n";
	} else {
		$seh=$seh."\n";
	}
	$tarb=0;
	print $socket "$seh";

	$ha=0;
	$SIG{ALRM} = sub { &slog("# Not recieving data!\n"); &v3_lwpback; exit 0;};
	alarm 10;
	while (<$socket>) {
		alarm 0;
		if ($ha==3) {
			$data_in=$data_in+length($_);
			&v3_print($_);
		} else {chop;}
		if ($ha==0) {
			&slog("\$ Connected to server.\n");
			&slog("* Request Sent:\n");
			&smog($seh,'>');
			if (lc(substr($_,0,5)) ne 'http/') {
				&slog("* That doesn't look like an HTTP header... uh oh... better quit (closing socket).\n");
				close $socket;
				&v3_loc("inv_head");
			}
			$hc=&errcode($_);
			if (substr($hc,0,1) eq '2') {
				&slog("* Got 2xx code: ");
				$ha=2;
			} elsif (($hc eq '100') and (lc($http_method) eq 'post')) {
				&slog("* 100 from post\n");
				$ha=9;
			} else {
				&slog("* Not 2xx code: ");
				$ha=1;
			}
		}
		if ($ha==9) {
			if ($_ eq "\r") {
				#$scarb=0;
				&slog("* Got Continue..\n");
				&smog($hout);
				$hout='';
				$ha=0;
				next;
			} else {$hout=$hout.$_."\n";}
		}
		if ($ha==1) {
			if ($_ eq "\r") {
				$tarb=1;
				&slog("Got header:\n");
				&smog($hout);
				&v3_headcook($hout);
				&slog("* Closing Socket...\n");
				close $socket;
			} else {$hout=$hout.$_."\n";}
		}
		if ($ha==2) {
			if ($_ eq "\r") {
				&slog("Got header:\n");
				&smog("$hout");
				&slog("* Processing...\n");
				&v3_headcook($hout);
				$dc = &v3_headcon($hout,'Content-type');
				@dv = split (':!!:',$dc);
				if (scalar(@dv)<1) {
					&slog("* Can't retrieve content-type..\n");
					&v3_loc("inv_head");
				}
				$lctype=$dv[0];
				@dx = split (';',$dv[0]);

				$wego=1;
				if (lc($field_r{$options{'v3rs_type'}}) ne 'file') {
					@ng=split('/',lc($dx[0]));
					if (($ng[0] eq 'audio') or ($ng[0] eq 'video') or ($ng[0] eq 'application')) {
						$wego=0;
						&v3_filedownload;
						exit 0;
					} elsif ((lc($dx[0]) eq 'text/html') or (lc($dx[0]) eq 'text/htm')) {$wego=2;}
				}

				if ($wego==1) {
					&slog("* Not text/html or FILE specified. Sendind raw file:\n");
					&v3_log('file');
					print &v3_setcookie;
					print "Content-Type: $dv[0]\n";
					$fc = &v3_headcon($hout,'Content-Length');
					@fv = split (':!!:',$fc);
					if ($fv[0] ne '') {
						print "Content-Length: $fv[0]\n";
					}
					$fc = &v3_headcon($hout,'Transfer-Encoding');
					@fv = split (':!!:',$fc);
					if ($fv[0] ne '') {
						print "Transfer-Encoding: $fv[0]\n";
					}
					$fc = &v3_headcon($hout,'Content-Encoding');
					@fv = split (':!!:',$fc);
					if ($fv[0] ne '') {
						print "Content-Encoding: $fv[0]\n";
					}
					print "\n";
					$ha=3;
				} elsif ($wego==2) {
					###### text/html STUFF
					&slog("* Found text/html - processing html code:\n");
					&v3_log('page');
					&v3_htmlStart;
					$ha=4;
					$fc = &v3_headcon($hout,'Transfer-Encoding');
					@fv = split (':!!:',$fc);
					if (lc($fv[0]) eq 'chunked') {
						&slog("* body is chunked..\n");
						$ha=5;
					}
					&slog("; ".$cookies."\n");
					print &v3_setcookie;
					print "Content-Type: text/html\n\n";
					&v3_print("<!--\nv3 [by: Jeremy Goodwin]\nAddress: $http_address\nReferer: $referer\n-->\n");

					### put in window popup script
					&v3_print("<script>\nif (!parent.v3ad) {window.open('".$options{'script_base_dir'}.$options{'script_name_v3_page'}."?type=ad','v3pu','width=468,height=90');}\n//</script>\n");


				}
			} else {$hout=$hout.$_."\n";}
		}
		if ($ha==4) {
			$pjt=$_;
			&v3_htmlPro("$pjt\n");
		}
		if ($ha==5) {
			$pjt=$_;
			$pjt=~s/\r//ig;
			if (($pjt ne '') and ($pjt ne '0')) {
				$chunk_to=hex($pjt);
				$chunk_cnt=0;
				$ha=6;
				#&slog('� New chunk.'." Length: $chunk_to\n");
			}
			if ($pjt eq '0') {
				#&slog("\n".'� Last chunk specified. Closing socket...'."\n");
				close $socket;
			}
		} elsif ($ha==6) {
			if ((substr($_,length($_)-1,1) eq "\r") and ((length($_)-1+$chunk_cnt)==$chunk_to)) {
				&v3_htmlPro(substr($_,0,length($_)-1));
				#&slog("\n".'� End of chunk.'."\n");
				$ha=5;
			} else {
				$chunk_cnt=$chunk_cnt+length($_)+1;
				&v3_htmlPro("$_\n");
			}
		}
			
	}
	close $socket;
	if (($ha>=4) and ($ha<=6)) {&v3_htmlFinish;}
	if ($ha>2) {
		&v3_datalog;
		&slog("\n# Data Out = $data_out\n# Data In = $data_in");

		if ($ads_seen != 0) {&v3_banner_add;}

		&slog("\n# Finished processing file... exiting!\n");
		exit 0;
	}
	if (($ha==1) and ($tarb==0)) {
		&slog("\n");
		&smog("$hout");
	}

	return $hout;
}

##############

sub errcode {
	if (substr($_[0],0,5) eq 'HTTP/') {
		@ni=split(' ',$_[0]);
		return $ni[1];
	} else {return false;}
}

##############

sub v3_headcon {
	@ua=split("\n",$_[0]);
	$ub=$_[1];
	$uout='';
	foreach $uc (@ua) {
		if (substr(lc($uc),0,length($ub)+1) eq (lc($ub).':')) {
			$ue=substr($uc,length($ub)+1,length($uc)-length($ub)-1);
			$uf='';
			$ud=0;
			for ($ug=0;$ug<length($ue);$ug++) {
				if ($ud==0) {
					if (substr($ue,$ug,1) ne ' ') {
						$ud=1;
						$uf=$uf.substr($ue,$ug,1);
					}
				} else {$uf=$uf.substr($ue,$ug,1);}
			}
			$uf=~s/\r//ig;
			$uout = $uout . $uf . ":!!:";
		}
	}
	return $uout;
}

##############

sub v3_SameServer {
	($s0,$q,$q,$q,$q)=&v3_getdd($_[0]);
	($s1,$q,$q,$q,$q)=&v3_getdd($_[1]);
	if ($s1 eq $s0) {return true;} else {return false;}
}

##############

sub v3_relurl {
	$xk=$_[0];
	$xk=~s/\n//ig;
	$xk=~s/\r//ig;
	$aa=0;
	if (lc(substr($xk,0,6)) eq 'ftp://') {$aa=1;}
	if (lc(substr($xk,0,9)) eq 'gopher://') {$aa=1;}
	if (lc(substr($xk,0,8)) eq 'https://') {$aa=1;}
	if (lc(substr($xk,0,7)) eq 'file://') {$aa=1;}
	if ($aa==1) {return $xk;}
	if (substr($xk,0,2) eq '//') {$xs='http:'.$xk;
	} elsif (substr(lc($xk),0,7) eq 'http://') {$xs=$xk;
	} elsif (substr($xk,0,1) eq '/') {$xs=$http_domain.substr($xk,1,length($xk)-1);
	} else {$xs=$http_dir.$xk;}

	if (substr(lc($xs),0,7) eq 'http://') {
	  $xbeg='';
	  $xend='';
	  $xst=0;
	  $xo=substr($xs,7,length($xs)-7);
	  for ($xl=0;$xl<length($xo);$xl++) {
	    if ($xst==0) {
	      if (substr($xo,$xl,1) eq '?') {
	        $xend=$xend.'?';
	        $xst=1;
	      } else {$xbeg=$xbeg.substr($xo,$xl,1);}
	    } else {$xend=$xend.substr($xo,$xl,1);}
	  }
	  $min=0;
	  if (substr($xbeg,length($xbeg)-1,1) eq '/') {
	    $xbeg=$xbeg.'/IGNORE';
	    $min=1;
	  }
	  @xi=split('/',$xbeg);
	  for ($xl=1;$xl<scalar(@xi)-$min;$xl++) {
	    if ($xi[$xl] eq '..') {
	      for ($xd=$xl-1;$xd>=1;$xd--) {
	        if ($xi[$xd] ne 'v3/') {
	          $xi[$xd]='v3/';
	          last;
	        }
	      }
	      $xi[$xl]='v3/';
	    }
	    if ($xi[$xl] eq '.') {$xi[$xl]='v3/';}
	  }
	  $xub='';
	  for ($xl=0;$xl<scalar(@xi)-$min;$xl++) {
	    if ($xi[$xl] ne 'v3/') {$xub=$xub.'/'.$xi[$xl];}
	  }
	  if (scalar(split('/',$xub))<=1) {$xub=$xub.'/';}
	  $xs='http:/'.$xub.$xend;
	}
	return $xs;
}

##############

sub v3_buildauth {
  $kdat=$_[0];
  $knew='';
  $kp=0;
  for ($kc=0;$kc<length($kdat);$kc++) {
    $kt=substr($kdat,$kc,1);
    #
    if ($kp==0) {
      if (($kt ne ' ') and ($kt ne "\n")) {
        $kp=1;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==1) {
      if (($kt eq ' ') or ($kt eq "\n")) {
        $kp=2;
        $knew="$knew <!@> ";
      } else {
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==2) {
      if (($kt ne ' ') and ($kt ne "\n")) {
        $kp=3;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==3) {
      if (($kt ne ' ') and ($kt ne '=') and ($kt ne "\n")) {$knew="$knew$kt";}
      if (($kt eq ' ') or ($kt eq "\n")) {
        $kp=2;
        $knew="$knew <!@> ";
      }
      if ($kt eq '=') {
        $kp=4;
        $knew="$knew =!= ";
      }
    next;
    }
    #
    if ($kp==4) {
      if ($kt eq '"') {
        $kp=6;
      } else {
        $kp=5;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==5) {
      if (($kt eq ' ') or ($kt eq "\n")) {
        $kp=2;
        $knew="$knew <!@> ";
      } else {
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==6) {
      if ($kt eq "\n") {$kt='';}
      if ($kt eq '"') {
        $kp=2;
        $knew="$knew <!@> "; 
      } else {
        $knew="$knew$kt";
      }     
    next;
    }
  }

  $knew="$knew <!@> ";
  #TAG STRING PROCESS END
  return "$knew";
}

###############

sub v3_searchauth {
	@sa = split(' <!@> ',$_[0]);
	foreach $sb (@sa) {
		@sc = split(' =!= ',$sb);
		if (lc($sc[0]) eq lc($_[1])) {
			return $sc[1];
		}
	}
	return false;
}

###############

sub v3_authpage {
	&slog("\n# Script Exit (show auth page)...\n");

	if (lc($http_method) eq 'post') {
		if ($http_address=~/\?/) {
			$http_address = $http_address . '&' . $post_content;
		} else {
			$http_address = $http_address . '?' . $post_content;
		}	
	}

	$peg = `$options{'local_base'}$options{'script_name_v3_page'} p auth`;
	if ($_[1] ne 'i') {$sa_details='';
	} else {$sa_details="<b>The login details you provided are not correct. Please try again:<br></b>\n";}
	$sa_form="<form action=\"$options{'script_base_dir'}$options{'script_name_v3_r'}\" method=get><input type=hidden name=\"$options{'string_r'}$options{'v3rs_type'}\" value=\"get\"><input type=hidden name=\"$options{'string_r'}$options{'v3rs_var'}\" value=\"$options{'misc_enc'}".&v3_e64($http_address)."\"><input type=hidden name=\"$options{'string_r'}$options{'v3rs_referer'}\" value=\"$options{'misc_enc'}".&v3_e64($referer)."\"><input type=hidden name=\"$options{'string_r'}$options{'v3rs_auth'}\" value=\"pos:".&v3_e64($_[0])."\">";
	$sa_login="<input type=text size=30 name=\"$options{'string_r'}$options{'v3rs_auth_userid'}\" value=\"\" class=sfield>";
	$sa_password="<input type=password size=30 name=\"$options{'string_r'}$options{'v3rs_auth_passwd'}\" value=\"\" class=sfield>";

	$peg=~s/#\]realm/$_[0]/ig;
	$peg=~s/#\]form/$sa_form/ig;
	$peg=~s/#\]details/$sa_details/ig;
	$peg=~s/#\]user/$sa_login/ig;
	$peg=~s/#\]pass/$sa_password/ig;

	print "Content-Type: text/html\n\n";
	print $peg;
	
	exit 0;
}

###############

sub v3_invauth {
	&v3_authpage($_[0],'i');
}

###############

sub v3_CookieStore {
	$ca=&v3_buildCookie($_[0]);
	@cb=split(':#;',$ca);
	$cexp='';
	$cdom='';
	$cpath='';
	$ccon='';
	foreach $cz (@cb) {
		@cx=split(':=:',$cz);
		if (lc($cx[0]) eq 'expires') {$cexp=$cx[1]; next;}
		if (lc($cx[0]) eq 'domain') {$cdom=$cx[1]; next;}
		if (lc($cx[0]) eq 'path') {$cpath=$cx[1]; next;}
		if (lc($cx[0]) eq 'secure') {return false;}
		$ccon=$ccon.$cx[0].':=:'.$cx[1].':#;';		
	}
	if ($ccon ne '') {$ccon=substr($ccon,0,length($ccon)-3);}
	if ($cpath eq '') {
		$cpath='/';
	} elsif (substr($cpath,0,1) ne '/') {
		$cpath='/'.substr($http_dir,length($http_dom),length($http_dir)-length($http_dom)).$cpath;
	}
	if (substr($cpath,length($cpath)-1,1) ne '/') {$cpath=$cpath.'/';}
	$cexp=&v3_sortdate($cexp);
	$cdom=&v3_sortdom($cdom);
	@ni=split(':#;',$ccon);
	foreach $in (@ni) {
		@cz=split(':=:',$in);
		&v3_addCookie("$cdom;$cpath;$cexp;$cz[0];$cz[1]");
	}

}

##########

sub v3_addCookie {
	@wa=split(':#;',$cookies);
	$wf=$_[0];
	@wo=split(";",$wf);
	$wc='';
	$wp=0;
	foreach $wz (@wa) {
		@wb=split(';',$wz);
		if (($wb[0] eq $wo[0]) and ($wb[1] eq $wo[1]) and ($wb[3] eq $wo[3])) {
			$wp=1;
			$wc=$wc.$wf.':#;';
		} else {
			$wc=$wc.$wz.':#;';
		}
	}
	if ($wp==0) {$wc=$wc.$wf.':#;';}
	if ($wc ne '') {$wc=substr($wc,0,length($wc)-3);}
	$cookies=$wc;
}

##########

sub v3_checkCookies {
	@wa=split(':#;',$cookies);
	$wc='';
	$wd=&v3_date;
	foreach $wz (@wa) {
		@wb=split(';',$wz);
		if (($wb[2]>$wd) or ($wb[2] eq '')) {
			$wc=$wc.$wz.':#;';
		}
	}
	if ($wc ne '') {$wc=substr($wc,0,length($wc)-3);}
	$cookies=$wc;
}

##########

sub v3_CookieOut {
	@wa=split(':#;',$cookies);
	$wc='';
	$wp='/'.substr($http_dir,length($http_dom),length($http_dir)-length($http_dom));
	$wnu=0;
	foreach $wz (@wa) {
		@wb=split(';',$wz);
		if (substr(lc($http_server),length($http_server)-length($wb[0]),length($wb[0])) eq lc($wb[0])) {
			if (length($wp)<length($wb[1])) {next;}
			if (substr(lc($wp),0,length($wb[1])) eq lc($wb[1])) {
				$num=scalar(split('/',$wb[1]));
				if ($num>$wnu) {$wnu=$num;}
				$wc=$wc.$num.';'.$wb[3].'='.$wb[4].':#;';
			}
			
		}
	}
	if ($wc ne '') {$wc=substr($wc,0,length($wc)-3);
	} else {return '';}
	@wa=split(':#;',$wc);
	$wc='';
	for ($kl=$wnu;$kl>=0;$kl--) {
		foreach $wz (@wa) {
			@wb=split(';',$wz);
			if ($wb[0]==$kl) {$wc=$wc.$wb[1].'; ';}
		}
	}
	if ($wc ne '') {$wc=substr($wc,0,length($wc)-2);}
	return $wc;
}

##########

sub v3_sortdom {
	$sd=$_[0];
	if (substr($sd,0,1) eq '.') {$sd=substr($sd,1,length($sd)-1);}
	$se=$sd;
	$se=~s/\./\;/g;
	@sa=split(';',$se);
	for ($sl=0;$sl<(scalar(@sa)-1);$sl++) {
		if ($sa[$sl] eq '') {return $http_server;}
	}
	$tld=lc($sa[scalar(@sa)-1]);
	if (scalar(@sa)<2) {return $http_server;}
	return $sd;
}

##########

sub v3_sortdate {
	if ($_[0]=~/(.*?), (.*?) (.*?) GMT/i) {
		@da=split('-',$2);
		if (length($da[2])==2) {
			if (substr($da[2],0,1) > 7) {$da[2]='19'.$da[2];
			} else {$da[2]='20'.$da[2];}
		}
		if (length($da[0])==1) {$da[0]='0'.$da[0];}
		if (lc(substr($da[1],0,3)) eq 'jan') {$da[1]='01';}
		if (lc(substr($da[1],0,3)) eq 'feb') {$da[1]='02';}
		if (lc(substr($da[1],0,3)) eq 'mar') {$da[1]='03';}
		if (lc(substr($da[1],0,3)) eq 'apr') {$da[1]='04';}
		if (lc(substr($da[1],0,3)) eq 'may') {$da[1]='05';}
		if (lc(substr($da[1],0,3)) eq 'jun') {$da[1]='06';}
		if (lc(substr($da[1],0,3)) eq 'jul') {$da[1]='07';}
		if (lc(substr($da[1],0,3)) eq 'aug') {$da[1]='08';}
		if (lc(substr($da[1],0,3)) eq 'sep') {$da[1]='09';}
		if (lc(substr($da[1],0,3)) eq 'oct') {$da[1]='10';}
		if (lc(substr($da[1],0,3)) eq 'nov') {$da[1]='11';}
		if (lc(substr($da[1],0,3)) eq 'dec') {$da[1]='12';}
		$yd=$da[2].$da[1].$da[0].substr($3,0,2).substr($3,3,2);
		if (length($yd)!=12) {return '';
		} else {return $yd;}
	}
	return '';
}

##########

sub v3_buildCookie {
  $kdat=$_[0];
  $knew='';
  $kp=2;
  for ($kc=0;$kc<length($kdat);$kc++) {
    $kt=substr($kdat,$kc,1);
    #
    if ($kp==2) {
      if (($kt ne ' ') and ($kt ne ";")) {
        $kp=3;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==3) {
      if (($kt ne ';') and ($kt ne '=')) {$knew="$knew$kt";}
      if ($kt eq ';') {
        $kp=2;
        $knew="$knew:#;";
      }
      if ($kt eq '=') {
        $kp=5;
        $knew="$knew:=:";
      }
    next;
    }
    #
    if ($kp==5) {
      if ($kt eq ';') {
        $kp=2;
        $knew="$knew:#;";
      } else {
        $knew="$knew$kt";
      }
    next;
    }
  }
  return $knew;
}

############

sub v3_date {
	$j=`date +%Y%m%d%k%M`;
	chomp($j);
	if (length($j)==12) {return $j;
	} else {return 0;}
}

############

sub v3_headcook {
	### Process Cookies
	$mc = &v3_headcon($_[0],'Set-Cookie');
	@mv = split (':!!:',$mc);
	if (scalar(@mv)>0) {
		foreach $mx (@mv) {
			&v3_CookieStore($mx);
			&v3_checkCookies;
			&slog("+ Found cookie: $mx\n");
		}
	}
}

###########

sub v3_setcookie {
	###return 'Set-Cookie: '.$options{'cookie_string'}.'='.&v3_e64($cookies).'; domain='.$options{'cookie_domain'}.'; path='.$options{'cookie_path'}.'; expires='.$options{'cookie_expires'}."\n";
	return 'Set-Cookie: '.$options{'cookie_string'}.'='.&v3_e64($cookies)."\n";

}

###########

sub v3_options {
	open(SF,"v3-config/options");
	  @opf=<SF>;
	close(SF);
	foreach $opd (@opf) {
		$opd=~s/\r/ /ig;
		$opd=~s/\t/ /ig;
		$opn='';
		$opc='';
		$ops=0;
		for ($opz=0;$opz<length($opd);$opz++) {
			$ope=substr($opd,$opz,1);
			if ($ope eq '#') {last;}
			if (($ops==0) and ($ope ne ' ')) {
				$opn=$ope;
				$ops=1;
				next;
			}
			if ($ops==1) {
				if ($ope eq ' ') {$ops=2;
				} else {$opn=$opn.$ope;}
				next;
			}
			if ($ops==2) {
				if (($ope ne ' ') and ($ope ne '=')) {
					$opc=$ope;
					$ops=3;
					next;
				}
			}
			if ($ops==3) {
				if ($ope eq ';') {
					$ops=4;
					last;
				} else {$opc=$opc.$ope;}
			}
		}
		$opn=lc($opn);
		if ($ops==4) {
			$options{$opn}=$opc;
		}
	}
}

###########

sub tagger {
  $kdat=$_[0];
  $kdat=~s/\t/ /ig; ## remove tabs
  $knew='';
  $kp=0;
  for ($kc=0;$kc<length($kdat);$kc++) {
    $kt=substr($kdat,$kc,1);
    #
    if ($kp==0) {
      if (($kt ne ' ') and ($kt ne "\n")) {
        $kp=1;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==1) {
      if (($kt eq ' ') or ($kt eq "\n")) {
        $kp=2;
        $knew="$knew <!@> ";
      } else {
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==2) {
      if (($kt ne ' ') and ($kt ne "\n")) {
        $kp=3;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==3) {
      if (($kt ne ' ') and ($kt ne '=') and ($kt ne "\n")) {$knew="$knew$kt";}
      if (($kt eq ' ') or ($kt eq "\n")) {
        $kp=2;
        $knew="$knew <!@> ";
      }
      if ($kt eq '=') {
        $kp=4;
        $knew="$knew =!= ";
      }
    next;
    }
    #
    if ($kp==4) {
      if (($kt eq '"') or ($kt eq "'")) {
	$kqt=$kt;
        $kp=6;
      } else {
        $kp=5;
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==5) {
      if (($kt eq ' ') or ($kt eq "\n")) {
        $kp=2;
        $knew="$knew <!@> ";
      } else {
        $knew="$knew$kt";
      }
    next;
    }
    #
    if ($kp==6) {
      if ($kt eq "\n") {$kt='';}
      if ($kt eq $kqt) {
        $kp=2;
        $knew="$knew <!@> "; 
      } else {
        $knew="$knew$kt";
      }     
    next;
    }
  }

  if (($knew ne '') and ($kp>1) and ($kp<4)) {$knew=substr($knew,0,length($knew)-6);}
  #TAG STRING PROCESS END
  return "$knew <old.> $kdat";
}

###########

sub tagtype {
  @jlo = split (' <old.> ',$_[0]);
  @juse = split(' <!@> ',$jlo[0]);
  return lc($juse[0]);
}

###########

sub rebuildtag {
  if ($_[1] ne '') {$niq=1;
	} else {$niq=0;} 
  $fre='<';
  @fuse = split(' <old.> ',$_[0]);
  @fusd = split(' <!@> ',$fuse[0]);
  foreach $fd (@fusd) {
    @fr=split(' =!= ',$fd);
    if ($fr[1] ne '') {$fre="$fre$fr[0]=\"$fr[1]\" ";
      } else { $fre="$fre$fd ";}
  }
  $fre=substr($fre,0,length($fre)-1);
  $fre="$fre>";
  if ($niq==1) {$fre=~s/\"//ig;}
  return $fre;
}

###########

sub v3_htmlPro {

  $data_in=$data_in+length($_[0])-1;

  $jdat=$_[0];
  $jdat=~s/\r//g;
  for ($jc=0;$jc<length($jdat);$jc++) {
    $hpd++;

    if (($jcom!=1) and ($tnew==1) and (substr($jdat,$jc,3) eq '!--')) {
      $jcom=1;
      $jstate[$jnum]='comment';
      $jc=$jc+3;
    }

    $jt=substr($jdat,$jc,1);

    if ($jcom==1) {
      if (substr($jdat,$jc,3) eq '-->') {
        $jcom=0;
        $jc=$jc+2;
        $jnum++;
        $jstate[$jnum]='text';
        $jcont[$jnum]='';
      } else {
        $jcont[$jnum]="$jcont[$jnum]$jt";
      }
    } else {

      if (($jt ne '<') and ($jt ne '>')) {
        $jcont[$jnum]="$jcont[$jnum]$jt";
        if ($tj ne ' ') {$tnew=0;}
      }

      ### print text if not in tag
      # put a check for $hpd for buffering [eg.. and ($hpd=>50) ]
      #
      if (($jstate[$jnum] eq 'text')  and ($jnum==0)) {
        &v3_print($jcont[0]);
        $jnum=0;
        $jstate[0]='text';
        $jcont[0]='';
        $tnew=0;
        $hpd=0;
      }

      if ($jt eq '>') {
        if ($jstate[$jnum] eq 'text') {$jcont[$jnum]="$jcont[$jnum]>";}
        if ($jstate[$jnum] eq 'tag') {
        
 	  $jcont[$jnum]=&tagger($jcont[$jnum]);

          if (&tagtype($jcont[$jnum]) eq 'script') {
            $tscr=1;
          }
          if (&tagtype($jcont[$jnum]) eq '/script') {
            $tscr=0;
            $sp=0;
            for ($z=0;$z<$jnum;$z++) {
              if (($jstate[$z] eq 'tag') and (&tagtype($jcont[$z]) eq 'script')) {
                $sp=1;
                next;
              }
              if ($sp==1) {
                if ($jstate[$z] eq 'text') {$jstate[$z]='script';}
                if ($jstate[$z] eq 'comment') {
                  $jstate[$z]='script';
                  $jcont[$z]="<!--$jcont[$z]-->";
                }
                if ($jstate[$z] eq 'tag') {
                  $jstate[$z]='script';
                  @a=split(' <old.> ',$jcont[$z]);
                  $jcont[$z]="<$a[1]>";
                }
              }
            }
          }
          
          if ($tscr==0) {
            for ($ez=0;$ez<($jnum+1);$ez++) {
		#########
		##print "$jstate[$ez]: $jcont[$ez]\n";
		#########
		if ($jstate[$ez] eq 'text') {&v3_print($jcont[$ez]);}
		if (($jstate[$ez] eq 'script') and (lc($no_script) ne 'on')) {
			#$hyp = &v3_oldpro($jcont[$ez]);
			$hyp = &v3_jspro($jcont[$ez]);
			&v3_print($hyp);
		}
		if ($jstate[$ez] eq 'comment') {&v3_print("<!--$jcont[$ez]-->");}
		if ($jstate[$ez] eq 'tag') {
			$out = &v3_dotag($jcont[$ez]);
			$nez = &rebuildtag($jcont[$ez]);
			if (lc($nez) ne lc($out)) {
				&v3_print($out);
			} else {
				@sez=split(' <old.> ',$jcont[$ez]);
				&v3_print("<$sez[1]>");
			}
		}
            }
            $jnum=0;
            $jstate[0]='text';
            $jcont[0]='';
            $tnew=0;
          } else {
            $jnum++;
            $jstate[$jnum]='text';
            $jcont[$jnum]='';
            $tnew=0;
          } 

        }
      }
      if ($jt eq '<') {
        if ($jstate[$jnum] eq 'tag') {
          $jstate[$jnum]='text';
          $jcont[$jnum]="<$jcont[$jnum]";
        }
        if ($jstate[$jnum] eq 'text') {
          $jnum++;
          $jstate[$jnum]='tag';
          $jcont[$jnum]='';
          $tnew=1;
        }
      }
    }


  }

}

##########

sub v3_htmlFinish {
  return;
  for ($ez=0;$ez<($jnum+1);$ez++) {
    if ($jstate[$ez] eq 'text') {&v3_print($jcont[$ez]);}
	if (($jstate[$ez] eq 'script') and (lc($no_script) ne 'on')) {
		$hyp = &v3_jspro($jcont[$ez]);
		&v3_print($hyp);
	}
	if ($jstate[$ez] eq 'comment') {&v3_print("<!--$jcont[$ez]-->");}
	if ($jstate[$ez] eq 'tag') {
		$out = &v3_dotag($jcont[$ez]);
		if ($out ne '<>') {
			$nez = &rebuildtag($jcont[$ez]);
			if (lc($nez) ne lc($out)) {
				&v3_print($out);
			} else {
				@sez=split(' <old.> ',$jcont[$ez]);
				&v3_print("<$sez[1]>");
			}
		}
	}
    }
}

##########

sub v3_htmlStart {
	$hpd=0;
	$jstate[0]='text';
	$jcont[0]='';
	$jnum=0;
	$tnew=0;
	$tscr=0;
	$send_op='';
	$send_opf='';
	$send_auth='';
	$send_apos='';
	$form_op='';

	@gq=keys(%field_o);
	foreach $gw (@gq) {
		$send_op=$send_op.'&'.$options{'string_o'}.$gw.'='.$field_o{$gw};
		$form_op=$form_op."<input type=\"hidden\" name=\"$options{'string_o'}$gw\" value=\"$field_o{$gw}\">";
	}
	$use_r='';
	for ($tj=0;$tj<length($http_address);$tj++) {
		$tw=substr($http_address,$tj,1);
		if ($tw eq '?') {last;
		} else {$use_r=$use_r.$tw;}
	}

	if ($titlei==0) {$tea=$options{'title_nor'};
	} else {$tea=$options{'title_lwp'};}
	$tiop='';
	if ($in_frame eq 'on') {$tiop=$tiop.' Inside_Frame;';}
	if ($no_script eq 'on') {$tiop=$tiop.' No_Java_Scripts;';}
	if ($ua_old eq 'on') {
		$tiop=$tiop.' Old_User_Agent;';
		$send_opf='&'.$options{'string_o'}.$options{'op_ua_old'}.'=on';
	}
	if ($enable_app eq 'on') {$tiop=$tiop.' App_Tags_Enabled;';}
	if ($tiop eq '') {$tiop='Options: None;';
	} else {$tiop='Options: '.$tiop;}
	if ($pux eq 'on') {$tiop='(Using_Proxy) | '.$tiop;}
	$tea=~s/!options/$tiop/ig;
	$tea=~s/!url/$http_address/ig;
	@tebb=split('!title',$tea);

	###
	# referer thingy
	###if ((substr(lc($field_r{$options{'v3rs_type'}}),0,4) eq 'form') or ($ml!=0)) {
		$send_op=$send_op.'&'.$options{'string_r'}.$options{'v3rs_referer'}.'='.$options{'misc_enc'}.&v3_e64($use_r);
		$send_opf=$send_opf.'&'.$options{'string_r'}.$options{'v3rs_referer'}.'='.$options{'misc_enc'}.&v3_e64($use_r);
		$form_op=$form_op."<input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_referer'}\" value=\"$options{'misc_enc'}".&v3_e64($use_r)."\">";
	###}

	###
	#$send_op=$send_op.'&p2bp_debug=on';
	###

	$form_tg = "<input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_type'}\" value=\"formget\">";

	$form_tp = "<input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_type'}\" value=\"formpost\">";

	if ($auth_type eq 'POS') {

		$send_auth='&'.$options{'string_r'}.$options{'v3rs_auth'}.'='.$options{'misc_enc'}.v3_e64($auth_realm.":!:".$auth_userid.":!:".$auth_passwd);

		$send_apos='&'.$options{'string_r'}.$options{'v3rs_auth'}.'=pos:'.&v3_e64($auth_realm).'&'.$options{'string_r'}.$options{'v3rs_auth_userid'}.'='.$options{'misc_enc'}.&v3_e64($auth_userid).'&'.$options{'string_r'}.$options{'v3rs_auth_passwd'}.'='.$options{'misc_enc'}.&v3_e64($auth_passwd);

		$form_apos="<input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_auth'}\" value=\"pos:".&v3_e64($auth_realm)."\"><input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_auth_userid'}\" value=\"".$options{'misc_enc'}.&v3_e64($auth_userid)."\"><input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_auth_passwd'}\" value=\"".$options{'misc_enc'}.&v3_e64($auth_passwd)."\">";

		$form_atry="<input type=\"hidden\" name=\"$options{'string_r'}$options{'v3rs_auth'}\" value=\"".$options{'misc_enc'}.&v3_e64($auth_realm.":!:".$auth_userid.":!:".$auth_passwd)."\">";

	}

	$send_apos=$send_apos.'&'.$options{'string_r'}.$options{'v3rs_ex'}.'=';
	$send_auth=$send_auth.'&'.$options{'string_r'}.$options{'v3rs_ex'}.'=';

	$send_req = $options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=get&'.$options{'string_r'}.$options{'v3rs_var'}.'=';

	$send_fil = $options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=file&'.$options{'string_r'}.$options{'v3rs_var'}.'=';

	

}

##########

sub v3_dotag {

	if ($_[1] ne '') {$niq=1;
	} else {$niq=0;}
	@fuse = split(' <old.> ',$_[0]);
	@fusd = split(' <!@> ',$fuse[0]);
	$qtn=lc($fusd[0]);
	$qf='';
	$qc='';
	$qk=0;
	foreach $qa (@fusd) {
		if ($qk==0) {
			$qk=1;
			next;
		}
		@qp=split(' =!= ',$qa);
		$qf=$qf.$qp[0].':#;';
		$qc=$qc.$qp[1].':#;';
		
	}

	if ($qf ne '') {$qf=substr($qf,0,length($qf)-3);}
	if ($qc ne '') {$qc=substr($qc,0,length($qc)-3);}

	@qtf=split(':#;',$qf);
	@qtc=split(':#;',$qc);

	$append='';
	$prepend='';
	$thide='';
	$ghide=0;

	foreach $qp (@process) {
		@qta = split (':;',$qp);

		## link processing (ie As and AREAs)
		if (($qtn eq lc($qta[0])) and ($qta[1] eq '%LINK')) {

			$qz=&v3_tagpos('href',@qtf);
			if ($qz!=-1) {
				$aa=0;
				if (lc(substr($qtc[$qz],0,2)) eq '//') {$qtc[$qz]='http:'.$qtc[$qz];}

				if (lc(substr($qtc[$qz],0,1)) eq '#') {$aa=3;}
				if (lc(substr($qtc[$qz],0,6)) eq 'ftp://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,9)) eq 'gopher://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,8)) eq 'https://') {$aa=1;}

				if (lc(substr($qtc[$qz],0,7)) eq 'mailto:') {$aa=3;
					if (lc($options{'allow_email'}) eq 'on') {
						$em_to='';
						$em_cc='';
						$em_subject='';
						$em_body='';
						$jtt=substr($qtc[$qz],7,length($qtc[$qz])-7);
						$jtt=~s/\?/&/ig;
						@jta=split('&',$jtt);
						for ($jtl=0;$jtl<scalar(@jta);$jtl++) {
							if ($jtl==0) {$em_to=$jta[0];
							} else {
								@jtb=split('=',$jta[$jtl]);
								if (lc($jtb[0]) eq 'cc') {$em_cc=$jtb[1];}
								if (lc($jtb[0]) eq 'subject') {$em_subject=$jtb[1];}
								if (lc($jtb[0]) eq 'body') {$em_body=$jtb[1];}
							}
						}
						if ($em_to ne '') {
							if ($em_cc ne '') {
								$em_cc=~tr/+/ /;
								$em_cc=~ s/%(..)/pack("c",hex($1))/ge;
								$em_cc=' '.$em_cc;
							}
							$em_to=~tr/+/ /;
							$em_to=~ s/%(..)/pack("c",hex($1))/ge;
							$em_to = '&to='.$options{'misc_enc'}.&v3_e64($em_to.$em_cc);
							if ($em_subject ne '') {
								$em_subject=~tr/+/ /;
								$em_subject=~ s/%(..)/pack("c",hex($1))/ge;
								$em_subject = '&subject='.$options{'misc_enc'}.&v3_e64($em_subject);
							}
							if ($em_body ne '') {
								$em_body=~tr/+/ /;
								$em_body=~ s/%(..)/pack("c",hex($1))/ge;
								$em_body = '&body='.$options{'misc_enc'}.&v3_e64($em_body);
							}
							$qtc[$qz] = $options{'script_base_dir'}.$options{'script_name_v3_page'}.'?type=email'.$em_to.$em_cc.$em_subject.$em_body;
						}
						$pz=&v3_tagpos('target',@qtf);
						if ($pz==-1) {
							push @qtf , 'TARGET';
							push @qtc , '_blank';
						} else {
							$qtc[$pz]='_blank';
						}

					}
				}

				if (lc(substr($qtc[$qz],0,5)) eq 'news:') {$aa=1;}
				if (lc(substr($qtc[$qz],0,11)) eq 'javascript:') {$aa=3;
					if ($no_script eq 'on') {
						$qtc[$qz]=$options{'script_base_dir'}.$options{'script_name_v3_page'}.'?type=p&p=no_javascript';
					} else {
						&v3_insertjs;
						$qtc[$qz]='javascript:'.&v3_jspro(substr($qtc[$qz],11,length($qtc[$qz])-11));
					}
				}
				if (lc(substr($qtc[$qz],0,7)) eq 'telnet:') {$aa=1;}
				if (lc(substr($qtc[$qz],0,5)) eq 'wais:') {$aa=1;}
				if (lc(substr($qtc[$qz],0,7)) eq 'file://') {$aa=1;}
				if ($aa==0) {
					if (lc(substr($qtc[$qz],0,7)) ne 'http://') {
						if (substr($qtc[$qz],0,1) eq '/') {
							$qtc[$qz]=substr($http_domain,0,length($http_domain)-1).$qtc[$qz];
						} else {
							$qtc[$qz]=$http_dir.$qtc[$qz];
						}
					}

					### imgad
					if ($qtn eq 'a') {
						if (&v3_SameServer($http_address,$qtc[$qz])) {
							$imgad=1;
						} elsif (length($qtc[$qz]) > 25) {
							$imgad=1;
						}
					}
					### end imgad

					$plota=$qtc[$qz];
					$enp=$options{'misc_enc'}.&v3_e64($qtc[$qz]);
					if (&v3_SameServer($http_address,$qtc[$qz])) {
						$qtc[$qz]=$send_req.$enp.$send_op.$send_apos;
					} else {
						$qtc[$qz]=$send_req.$enp.$send_op.$send_auth;
					}

					#if (lc($no_script) ne 'on') {
					#	$qz=&v3_tagpos('onmouseover',@qtf);
					#	if ($qz==-1) {
					#		push @qtf , 'OnMouseOver';
					#		push @qtc , "window.status='[v3.2: $plota]'; return true";
					#	} else {
					#		$qtc[$qz]=$qtc[$qz]."; window.status='[v3.2: $plota]'; return true";
					#	}
					#}
				}
				if ($aa==1) {
					$enp=$options{'misc_enc'}.&v3_e64($qtc[$qz]);
					$qtc[$qz]=$send_req.$enp.'&';
				}

			}
		}

		### url processing (ie. images)
		if (($qtn eq lc($qta[0])) and ($qta[1] eq '%URL')) {
			##do checks
			$al=1;
			if ($qta[4] ne '') {
				$al=0;
				@cq=split('=',$qta[4]);
				$qz=&v3_tagpos($cq[0],@qtf);
				if (($qz!=-1) and (lc($qtc[$qz]) eq lc($cq[1]))) {$al=1;}
				if (($qz!=-1) and ($cq[1] eq '')) {$al=1;}
			}

			$qz=&v3_tagpos($qta[2],@qtf);
			if (($qz!=-1) and ($al==1)) {
				$aa=0;

				if (lc(substr($qtc[$qz],0,2)) eq '//') {$qtc[$qz]='http:'.$qtc[$qz];}

				if (lc(substr($qtc[$qz],0,6)) eq 'ftp://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,9)) eq 'gopher://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,8)) eq 'https://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,7)) eq 'file://') {$aa=1;}
				if ($aa==0) {
					if (lc(substr($qtc[$qz],0,7)) ne 'http://') {
						if (substr($qtc[$qz],0,1) eq '/') {
							$qtc[$qz]=substr($http_domain,0,length($http_domain)-1).$qtc[$qz];
						} else {
							$qtc[$qz]=$http_dir.$qtc[$qz];
						}
					}
					$enp=$options{'misc_enc'}.&v3_e64($qtc[$qz]);
					if (&v3_SameServer($http_address,$qtc[$qz])) {
						if (lc($qta[3]) eq 'file') {$qtc[$qz]=$send_fil.$enp.$send_opf.$send_apos;
						} else {$qtc[$qz]=$send_req.$enp.$send_op.$send_apos;}
					} else {
						if (lc($qta[3]) eq 'file') {$qtc[$qz]=$send_fil.$enp.$send_opf.$send_auth;
						} else {$qtc[$qz]=$send_req.$enp.$send_op.$send_auth;}
					}
				}
				if ($aa==1) {
					if (lc($qta[3]) eq 'file') {
						$qtc[$qz]='';
						$qtf[$qz]='';	
					} else {
						$enp=$options{'misc_enc'}.&v3_e64($qtc[$qz]);
						$qtc[$qz]=$send_req.$enp.'&';
					}
				}
			}
		}

		## produce real urls
		if (($qtn eq lc($qta[0])) and ($qta[1] eq '%AUR')) {
			$qz=&v3_tagpos($qta[2],@qtf);
			if ($qz!=-1) {
				$aa=0;

				if (lc(substr($qtc[$qz],0,2)) eq '//') {$qtc[$qz]='http:'.$qtc[$qz];}

				if (lc(substr($qtc[$qz],0,6)) eq 'ftp://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,9)) eq 'gopher://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,8)) eq 'https://') {$aa=1;}
				if (lc(substr($qtc[$qz],0,7)) eq 'file://') {$aa=1;}
				if ($aa==0) {
					if (lc(substr($qtc[$qz],0,7)) ne 'http://') {
						if (substr($qtc[$qz],0,1) eq '/') {
							$qtc[$qz]=substr($http_domain,0,length($http_domain)-1).$qtc[$qz];
						} else {
							$qtc[$qz]=$http_dir.$qtc[$qz];
						}
					}
				}
			}
		}

		## hide tags
		if (($qtn eq lc($qta[0])) and ($qta[1] eq '%HID')) {
			$thide='';
			$ghide=1;
		}

		
	}


	### form stuff
	if ($qtn eq 'form') {
		$qz=&v3_tagpos('action',@qtf);
		if ($qz!=-1) {
			$scl=$qtc[$qz];
			$aa=0;

			if (lc(substr($scl,0,2)) eq '//') {$scl='http:'.$scl;}

			if (lc(substr($scl,0,6)) eq 'ftp://') {$aa=1;}
			if (lc(substr($scl,0,9)) eq 'gopher://') {$aa=1;}
			if (lc(substr($scl,0,8)) eq 'https://') {$aa=1;}
			if (lc(substr($scl,0,7)) eq 'file://') {$aa=1;}
			if ($aa==0) {
				if (lc(substr($scl,0,7)) ne 'http://') {
					if (substr($scl,0,1) eq '/') {
						$scl=substr($http_domain,0,length($http_domain)-1).$scl;
					} else {
						$scl=$http_dir.$scl;
					}
				}
			}
			$qtc[$qz]=$options{'script_base_dir'}.$options{'script_name_v3_r'};
			$tv=&v3_e64($scl);
			$scl="$options{'misc_enc'}$tv";
			#$scl=&v3_censor($scl);
			$append=$append.'<input type="hidden" name="'.$options{'string_r'}.$options{'v3rs_var'}.'" value="'.$scl.'">';
			$qh=&v3_tagpos('method',@qtf);
			if ($qh!=-1) {
				if (lc($qtc[$qh]) eq 'post') {$append=$append.$form_tp; ####!!!!
				} else {
					$append=$append.$form_tg;
					$qtc[$qh]='POST';
				}
			} else {
				$append=$append.$form_tg;
				push @qtf , 'method';
				push @qtc , 'POST';
			}
			$append=$append.$form_op; ##.'<input type=hidden name=p2bp_debug value=on>';
		}
	}

	### meta refreshing
	if ($qtn eq 'meta') {
		$qz=&v3_tagpos('http-equiv',@qtf);
		if (($qz!=-1) and (lc($qtc[$qz]) eq 'refresh')) {
			$qh=&v3_tagpos('content',@qtf);
			if ($qh!=-1) {
				$ft=&v3_buildCookie($qtc[$qh]);
				@fy=split(':#;',$ft);
				$fo='';
				foreach $fp (@fy) {
					@fm=split(':=:',$fp);
					if (lc($fm[0]) eq 'url') {
						if (lc(substr($fm[1],0,2)) eq '//') {$fm[1]='http:'.$fm[1];}
						$fm[1]=&v3_relurl($fm[1]);
						$enp=$options{'misc_enc'}.&v3_e64($fm[1]);
						if (&v3_SameServer($http_address,$fm[1])) {
							$fm[1]=$send_req.$enp.$send_op.$send_apos;
						} else {
							$fm[1]=$send_req.$enp.$send_op.$send_auth;
						}
						#$prepend=$prepend.'<!--';
						#$append=$append.'-->';
					}
					if ($fm[1] ne '') {$fo=$fo.$fm[0]."=".$fm[1]."; ";
					} else {
						###if ($fm[0] < 300) {$fm[0]=300;}
						$fo=$fo.$fm[0]."; ";
					}
				}
				if ($fo ne '') {
					$fo=substr($fo,0,length($fo)-2);
					$qtc[$qh]=$fo;
				}
			}
		}
	}

	## title editing
	if ($qtn eq 'title') {
		$append=$append.$tebb[0];
	}
	if ($qtn eq '/title') {
		$prepend=$prepend.$tebb[1];
	}

	## making body have colour if it doesn't
	if ($qtn eq 'body') {
		$qz=&v3_tagpos('bgcolor',@qtf);
		if ($qz==-1) {
			push @qtf , 'BGCOLOR';
			push @qtc , '#FFFFFF';
		}
	}

	## changing $http_dir and $http_dom if a base-href is found
	if ($qtn eq 'base') {
		$qz=&v3_tagpos('href',@qtf);
		if ($qz!=-1) {
			($ss,$http_domain,$http_dir,$ss,$ss)=&v3_getdd(&v3_relurl($qtc[$qz]));
			$qtf[$qz] = '';
			$qtc[$qz] = '';
		}
	}

	## remove src from a script tag
	if ($qtn eq 'script') {
		$qz=&v3_tagpos('src',@qtf);
		if ($qz!=-1) {
			$qtf[$qz] = '';
			$qtc[$qz] = '';
		}
	}

	## cancelling imgad
	if ($qtn eq '/a') {
		$imgad=0;
	}

	## set inside script status'
	if (($no_script ne 'on') and ($qtn eq 'script')) {
		&v3_insertjs;
		$inside_js=1;
	}
	if (($no_script ne 'on') and ($qtn eq '/script')) {$inside_js=0;}


	## disabling applets, objects, params and embeds
	if ($enable_app ne 'on') {
		if (($qtn eq 'object') or ($qtn eq 'applet') or ($qtn eq 'param') or ($qtn eq 'embed') or ($qtn eq '/object') or ($qtn eq '/applet') or ($qtn eq '/param') or ($qtn eq '/embed')) {
			$thide='';
			$ghide=1;
		}
	}

	## banner ad replacement
	if (($qtn eq 'img') and ($imgad==1) and (lc($options{'ba_replace'}) eq 'on')) {   ####  and ($in_frame ne 'on')
		$iih=-1;
		$iiw=-1;
		$qz=&v3_tagpos('height',@qtf);
		if ($qz!=-1) {$iih=$qtc[$qz];}
		$qz=&v3_tagpos('width',@qtf);
		if ($qz!=-1) {$iiw=$qtc[$qz];}
		$by=0;
		if (($iiw eq '468') and ($iih < 100) and ($iih > 29)) {$by=1;}
		if (($iiw eq '234') and ($iih eq '60')) {$by=1;}
		if (($iiw eq '420') and ($iih eq '60')) {$by=1;}
		if (($iiw eq '400') and ($iih eq '60')) {$by=1;}
		if (($iiw eq '400') and ($iih eq '40')) {$by=1;}
		if (($iiw eq '460') and ($iih eq '68')) {$by=1;}

		if ($by==1) {
			$ghide=1;
			$thide='</a><!-- BEGIN LINKEXCHANGE CODE --><iframe src="http://leader.linkexchange.com/3/X1511758/showiframe?" width='.$iiw.' height='.$iih.' marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no><a href="http://leader.linkexchange.com/3/X1511758/clickle" target="_top"><img width='.$iiw.' height='.$iih.' border=0 ismap alt="" src="http://leader.linkexchange.com/3/X1511758/showle?"></a></iframe><!-- END LINKEXCHANGE CODE --><a name="v3">';

			$imgad=0;
			$ads_seen++;
		}
	}

	#### all tags
	# remove all javascripts in tags if no java script option is on
	
	for ($nn=0;$nn<scalar(@qtf);$nn++) {
		if (substr(lc($qtf[$nn]),0,2) eq 'on') {
			if ($no_script eq 'on') {
				$qtf[$nn]='';
				$qtc[$nn]='';
			} else {
				&v3_insertjs;
				$qtc[$nn]=&v3_jspro($qtc[$nn]);
			}
		}
	}
	
	# change targets if in a frame
	if ($in_frame eq 'on') {
		$qz=&v3_tagpos('target',@qtf);
		if ($qz!=-1) {
			if (substr($qtc[$qz],0,1) eq '_') {
				$qtc[$qz] = 'v3frame';
			}
		}
	}
	#### all tags end

	$mout="<$qtn ";
	for ($qn=0;$qn<scalar(@qtf);$qn++) {
		if ($qtc[$qn] ne '') {$mout="$mout$qtf[$qn]=\"$qtc[$qn]\" ";
		} else { $mout="$mout$qtf[$qn] ";}
	}
	$mout=substr($mout,0,length($mout)-1);
	if ($niq==1) {$mout=~s/\"//ig;}
	$mout="$mout>$append";
	if ($ghide==0) {return "$prepend$mout";
	} else {return $thide;}

}

##########

sub v3_tagpos {
	for ($nn=1;$nn<scalar(@_);$nn++) {
		if (lc($_[$nn]) eq lc($_[0])) {return $nn-1;}
	}
	return -1;
}

##########

sub v3_censor {

	@parts = split("",$_[0]);
	$tn=0;
	$tmq='';
	foreach $piece (@parts) {
		if ($tn==2) {
			$tn=0;
			$tmq="$tmq$options{'null_1string'}$piece";
		} else {$tmq="$tmq$piece";}          
		$tn++;
	}
	return $tmq;
}

##########

sub v3_oldpro {
	# similar tag process used in v2 - (only used in javascripts for v3)
	$oii=$_[0];
	@dob=split("<",$oii);
	for ($dot=1;$dot<scalar(@dob);$dot++) {
		@doc=split(">",$dob[$dot]);
		$dod=$doc[0];
		$tag="<$dod>";
		$dom = &tagger($dod);	
		$dok = &rebuildtag($dom,'y');
		$dol = &v3_dotag($dom,'y');
		if (lc($dol) ne lc($dok)) {
			$tag=~s/\\/\\\\/g;
			$tag=~s/\+/\\\+/g;
			$tag=~s/\?/\\\?/g;
			$tag=~s/\./\\\./g;
			$tag=~s/\*/\\\*/g;  
			$tag=~s/\^/\\\^/g;  
			$tag=~s/\$/\\\$/g;
			$tag=~s/\(/\\\(/g;
			$tag=~s/\)/\\\)/g;
			$tag=~s/\[/\\\[/g;
			$tag=~s/\]/\\\]/g;
			$tag=~s/\{/\\\{/g;
			$tag=~s/\}/\\\}/g;
			$tag=~s/\|/\\\|/g;
			$oii=~s/$tag/$dol/;
		}
	}
	return $oii;
}

##########

sub v3_lwpback {

	&slog("\n");
	if ($pux eq 'on') {
		&slog("# Not using LWP because of proxy\n");
		&v3_loc('no_tcp');
		exit 0;
	} elsif ($pux eq 'onfail') {
		&slog("# ONFAIL: Going to attempt access to site using a proxy...\n");
		$pux='on';
		&v3_mainloop;
		exit 0;
	}

	### send error if lwp is disabled
	if (lc($options{'can_use_lwp'}) ne 'on') {
		&slog("# Not using LWP because it is disabled\n");
		&v3_loc('no_tcp');
		exit 0;
	}

	use LWP;
	use LWP::Simple;

	$titlei=1;

	&slog("# Using LWP for request. (addr = $http_address)\n");
	($http_server,$http_domain,$http_dir,$http_address,$http_port)=&v3_getdd($http_address);
	&slog("] Server: $http_server\n] Domain: $http_domain\n] Directory: $http_dir\n] New Address: $http_address\n] Port: $http_port\n");
	
	$SIG{ALRM} = sub { &slog("# All attempts failed!\n"); &v3_loc('no_tcp'); exit 0;};
	alarm 8;

	if (lc($http_method) eq 'post') {
		if ($http_address=~/\?/) {
			$http_address = $http_address . '&' . $post_content;
		} else {
			$http_address = $http_address . '?' . $post_content;
		}	
	}
	
	($content_type,$ss,$ss,$ss,$ss)=head($http_address);
	if (($content_type=~/text\/htm/i) or ($content_type eq '')) {
		$content_type='text/html';
		&v3_htmlStart;
	}
	###if (lc($field_r{$options{'v3rs_type'}}) eq 'file') {$content_type='';}

	$ua = new LWP::UserAgent;
	$ua->agent($options{'user_agent'} . $ua->agent);
	my $req = new HTTP::Request GET => $http_address;
	
	if ($auth_type eq 'POS') {
		$req->authorization_basic($auth_userid, $auth_passwd);
	}

	$kickback=0;
	
	my $res = $ua->request($req,
	sub {
		alarm 0;
		if ($content_type eq 'text/html') {
			if ($kickback==0) {
				print "Content-Type: $content_type\n\n";
				&v3_print("<!--\nv3.2 (using lwp) [by: Jeremy Goodwin]\nAddress: $http_address\n-->\n");
			}
			&v3_htmlPro($_[0]);
		} else {
			if ($kickback==0) {
				 print "Content-Type: $content_type\n\n";
			}
			print $_[0];
		}
		$kickback=1;
	});

	if ($kickback==0) {
		&slog("# Nothing sent using LWP!\n");
		&v3_loc('no_tcp');
		exit 0;
	}

	exit 0;

}

##########

sub v3_log {

	if (lc($_[0]) eq 'page') {$duv = 'page';
	} else {$duv = 'file';}

	if (lc($options{'logging'}) eq 'on') {

		if ($pux eq 'on') {$plux='on';
		} else {$plux='';}

		if (lc($http_method) eq 'post') {
			if ($http_address=~/\?/) {
				$logaddr = $http_address . '&' . $post_content;
			} else {
				$logaddr = $http_address . '?' . $post_content;
			}	
		} else {
			$logaddr = $http_address;
		}

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);
		$tim = `date +%T`;
		chomp ($tim);

		$logline = "$tim ;>> Proxy_Status;: $plux ; Address;: $logaddr ; Content-Type;: $lctype ; Method;: $http_method ; Referer;: $referer ; User_IP;: $ENV{'REMOTE_ADDR'} ; User_Host;: $ENV{'REMOTE_HOST'} ; User_UA;: $ENV{'HTTP_USER_AGENT'}";
		$logline=~s/\n/\\n/ig;
		$logline=~s/\r//ig;
		$logline=~s/<\/pre>/<\\\/pre>/ig;

		if (-e "$options{'log_dir'}$dub.$duv.log") {

			open (LOG,">>$options{'log_dir'}$dub.$duv.log");
				print LOG "$logline\n";
			close (LOG);
			&slog('@ Logged request details.'."\n");

		} else {

			$fulldate = `date +%A`;
			chomp ($fulldate);
			$fulldate = $fulldate . ', ' . `date +%d-%m-%Y`;
			chomp ($fulldate);
			open (LOG,">$options{'log_dir'}$dub.$duv.log");
				print LOG "# v3-log ($duv) > $fulldate\n";
				print LOG "$logline\n";
			close (LOG);
			chmod 0666,"$options{'log_dir'}$dub.$duv.log";
			&slog('@ Logged request details (new file).'."\n");

		}

	}

}

#########

sub error_log {

	if (lc($options{'logging'}) eq 'on') {

		if ($pux eq 'on') {$plux='on';
		} else {$plux='';}

		if (lc($http_method) eq 'post') {
			if ($http_address=~/\?/) {
				$logaddr = $http_address . '&' . $post_content;
			} else {
				$logaddr = $http_address . '?' . $post_content;
			}	
		} else {
			$logaddr = $http_address;
		}

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);
		$tim = `date +%T`;
		chomp ($tim);

		$logline = "$tim ;>> Error_Code;: $_[0] ; Proxy_Status;: $plux ; Address;: $logaddr ; Method;: $http_method ; Referer;: $referer ; User_IP;: $ENV{'REMOTE_ADDR'} ; User_Host;: $ENV{'REMOTE_HOST'} ; User_UA;: $ENV{'HTTP_USER_AGENT'}";
		$logline=~s/\n/\\n/ig;
		$logline=~s/\r//ig;
		$logline=~s/<\/pre>/<\\\/pre>/ig;

		if (-e "$options{'log_dir'}$dub.error.log") {

			open (LOG,">>$options{'log_dir'}$dub.error.log");
				print LOG "$logline\n";
			close (LOG);
			&slog('@ Logged error details.'."\n");

		} else {

			$fulldate = `date +%A`;
			chomp ($fulldate);
			$fulldate = $fulldate . ', ' . `date +%d-%m-%Y`;
			chomp ($fulldate);
			open (LOG,">$options{'log_dir'}$dub.error.log");
				print LOG "# v3-log (error) > $fulldate\n";
				print LOG "$logline\n";
			close (LOG);
			&slog('@ Logged error details (new file).'."\n");
			chmod 0666,"$options{'log_dir'}$dub.error.log";

		}

	}

}

###########

sub v3_filedownload {

	&slog("\n# File Intended for Download...\n");

	&v3_htmlStart;
	$savefile = $options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=file&'.$options{'string_r'}.$options{'v3rs_var'}.'='.$options{'misc_enc'}.&v3_e64($http_address).$send_opf;
	if ($auth_type eq 'POS') {$savefile=$savefile.$send_apos;}
	@ouy=split('/',$http_address);
	$ouu=$ouy[scalar(@ouy)-1];
	
	$peg = `$options{'local_base'}$options{'script_name_v3_page'} p file`;
	$peg=~s/#\]url/$http_address/ig;
	$peg=~s/#\]v3-r/$options{'script_name_v3_r'}/ig;
	$peg=~s/#\]save/$savefile/ig;
	$peg=~s/#\]file/$ouu/ig;

	print "Content-Type: text/html\n\n";
	print "<!-- Page Log:\n$trog-->\n";
	print $peg;
	
	exit 0;
}

###########

sub v3_insertjs {

	if (($js_p_c==0) and ($no_script ne 'on')) {
		$js_p_c=1;
		&v3_print("<script><!--\n");
		open (HE, $options{'local_base'}.'v3-pages/censor.jsr');
			while (<HE>) {
				$jezl=$_;
				$jezl=~s/##NULL/$options{'null_1string'}/ig;
				&v3_print($jezl);
			}
		close (HE);
		&v3_print("\n//--></script>");
	}

}

###########

sub v3_jspro {

	if ($no_script eq 'on') {return '';}

	$sja=$_[0];

	if ($enable_app eq 'on') {$re_enableapp=1;
	} else {$re_enableapp=0;}

	$jsr_location = $options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=jget&'.$options{'string_r'}.$options{'v3rs_var'}.'='.$options{'misc_enc'}.&v3_e64($http_dir).$send_op.$send_auth;
	$jsr_src = $options{'script_base_dir'}.$options{'script_name_v3_r'}.'?'.$options{'string_r'}.$options{'v3rs_type'}.'=jfile&'.$options{'string_r'}.$options{'v3rs_var'}.'='.$options{'misc_enc'}.&v3_e64($http_dir).$send_opf.$send_auth;

	if ($js_p_w==0) {
		if ($_[0]=~/\.write/i) {
			$js_p_w=1;
			if ($inside_js==0) {&v3_print("<script><!--\n");
			} else {&v3_print("//<--\n");}
			open (HE, $options{'local_base'}.'v3-pages/write.jsr');
				while (<HE>) {
					$jezl=$_;
					$jezl=~s/##NULL/$options{'null_1string'}/ig;
					$jezl=~s/##ENABLEAPP/$re_enableapp/ig;
					$jezl=~s/##HREF/$jsr_location/ig;
					$jezl=~s/##SRC/$jsr_src/ig;
					&v3_print($jezl);
				}
			close (HE);
			if ($inside_js==0) {&v3_print("\n//--></script>");
			} else {&v3_print("//-->\n");}

		}
	}

	$sjo='';	# final output
	$sjiq=0;	# in quote status
	$sjb=0;		# bracket level
	$sjs=0;		# status in processing
	$sjm='';	# temp string var
	$sjwr=0;


	# start loop
	for ($sjz=0;$sjz<length($sja);$sjz++) {
	
		$sjc=substr($sja,$sjz,1);	# create var of char (easy reference)
	
		if ($sjb < 0) {$sjb=0;}
	
		### if in comment - only escape on new line
		if ($sjs==60) {
			if ($sjc eq "\n") {
				$sjs=0;
				$sjo=$sjo."\n";
				next;
			} else {
				$sjo=$sjo.$sjc;
				next;
			}
		}
	
		### if in a quote, only escape if quote ended
		if ($sjiq==2) {
			if ($sjc eq "\'") {
				if (substr($sja,$sjz-1,1) eq "\\") {
					$sjo=$sjo.$sjc;
					next;
				} else {
					$sjo=$sjo.$sjc;
					$sjiq=0;
					next;
				}
			} else {
				$sjo=$sjo.$sjc;
				next;
			}
		} elsif ($sjiq==1) {
			if ($sjc eq "\"") {
				if (substr($sja,$sjz-1,1) eq "\\") {
					$sjo=$sjo.$sjc;
					next;
				} else {
					$sjo=$sjo.$sjc;
					$sjiq=0;
					next;
				}
			} else {
				$sjo=$sjo.$sjc;
				next;
			}
	
		### if not in quote check to see if quote is starting
		} elsif (($sjc eq "\"") or ($sjc eq "\'")) {
			if ($sjs==1) {
				$sjo=$sjo.$sjm.$sjc;
				$sjs=51;
				$sjm='';
			} elsif ($sjs==0) {
				$sjo=$sjo.$sjc;
				$sjs=50;
				$sjm=''		
			} else {$sjo=$sjo.$sjc;}
			if ($sjc eq "\'") {$sjiq=2;
			} else {$sjiq=1;}
			next;
		
	
		} else {
			## processing for not in quote
	
	
			### waiting for bracket level to drop to 0
			if (($sjs==50) and ($sjb<=0)) {$sjs=0;}
			if ($sjs==50) {
				if ($sjc eq '(') {
					$sjb++;
					$sjo=$sjo.$sjc;
					next;
				} elsif ($sjc eq ')') {
					$sjb--;
					$sjo=$sjo.$sjc;
					if ($sjb==0) {$sjs=0;}
					next;
				} else {
					$sjo=$sjo.$sjc;
					next;
				}
			}
	
			### waiting for end of command
			if ($sjs==51) {
				$sjo=$sjo.$sjc;
				if ($sjc eq '(') {
					$sjb++;
					if ($sjb==1) {$sjs=50;}
				} elsif ($sjc eq ')') {$sjb--;
				} elsif ($sjb==0) {
					if (($sjc eq '=') or ($sjc eq ';') or ($sjc eq "\n") or ($sjc eq '}') or ($sjc eq '{')) {
						$sjs=0;
					}
				}
				next;
			}
	

			### reset levels and status if a comment is found
			if (substr($sja,$sjz,2) eq '//') {
				$sjz++;
				if ($sjs==1) {
					$sjo=$sjo.$sjm.'//';
					$sjm='';
				} elsif ($sjs==25) {
					$sjo=$sjo.')//';
					$sjm='';
				} else {$sjo=$sjo.'//';}
				$sjs=60;
				next;
			}
			if (substr($sja,$sjz,4) eq '<!--') {
				$sjz=$sjz+3;
				if ($sjs==1) {
					$sjo=$sjo.$sjm.'<!--';
					$sjm='';
				} elsif ($sjs==25) {
					$sjo=$sjo.')<!--';
					$sjm='';
				} else {$sjo=$sjo.'<!--';}
				$sjs=60;
				$sjb=0;
				next;
			}

			### waiting for command
			if ($sjs==0) {
				if (($sjc eq '=') or ($sjc eq ' ') or ($sjc eq "\t") or ($sjc eq ';') or ($sjc eq '{') or ($sjc eq '}') or ($sjc eq "\n")) {
					### then not starting command
					$sjo=$sjo.$sjc;
					next;
				} elsif ($sjc eq '(') {
					### up bracket level
					$sjo=$sjo.$sjc;
					$sjs=50;
					$sjb=1;
					next;
				} else {
					### command starting
					$sjs=1;
					$sjm=$sjc;
					next;
				}
			}
	
			### in command type
			if ($sjs==1) {
				if (($sjc eq ';') or ($sjc eq "\n") or ($sjc eq '}') or ($sjc eq '{')) {
					$sjo=$sjo.$sjm.$sjc;
					$sjm='';
					$sjs=0;
					next;
				} elsif (($sjc eq "\t") or ($sjc eq ' ') or ($sjc eq '=') or ($sjc eq '(')) {
					$sjz--;
	
					if (lc($sjm) eq 'var') {
						$sjo=$sjo.$sjm;
						$sjm='';
						$sjs=0;
						next;
					}
					@sjy=split(/\./,$sjm);
					if ((lc($sjy[scalar(@sjy)-1]) eq 'open') or (lc($sjy[scalar(@sjy)-1]) eq 'replace')) {
						$sjo=$sjo.$sjm;
						$sjm='';
						$sjs=10;
						next;
					}
					if (lc($sjy[scalar(@sjy)-1]) eq 'src') {
						$sjo=$sjo.$sjm;
						$sjm='';
						$sjs=20;
						next;
					}
					if ((lc($sjy[scalar(@sjy)-1]) eq 'location') or (lc($sjy[scalar(@sjy)-1]) eq 'href')) {
						$sjo=$sjo.$sjm;
						$sjm='';
						$sjs=21;
						next;
					}
					if ((lc($sjy[scalar(@sjy)-1]) eq 'write') or (lc($sjy[scalar(@sjy)-1]) eq 'writeln')) {
						$sjo=$sjo.$sjm;
						$sjm='';
						$sjs=11;
						next;
					}
					$sjo=$sjo.$sjm;
					$sjm='';
					$sjs=51;
					next;
	
	
				} else {
					$sjm=$sjm.$sjc;
					next;
				}
			}
	
	
			### waiting for =
			if (($sjs==20) or ($sjs==21)) {
				if (($sjc eq ' ') or ($sjc eq "\t")) {
					$sjo=$sjo.$sjc;
					next;
				} elsif ($sjc eq '=') {
					if ($sjs==20) {$sjo=$sjo.'= '."\'".$jsr_src."\' + v3censor(";
					} else {$sjo=$sjo.'= '."\'".$jsr_location."\' + v3censor(";}
					$sjs=25;
					next;
				} else {
					$sjo=$sjo.$sjc;
					$sjs=51;
					next;
				}
			}
	
			### waiting for end of string before resetting
			if ($sjs==25) {
				if ($sjc eq '(') {
					$sjo=$sjo.$sjc;
					$sjb++;
					next;
				} elsif ($sjc eq ')') {
					$sjo=$sjo.$sjc;
					$sjb--;
					next;
				} elsif (($sjb==0) and (($sjc eq ';') or ($sjc eq "\n") or ($sjc eq '}') or ($sjc eq '{'))) {
						$sjo=$sjo.')'.$sjc;
						$sjs=0;
						next;
				} else {
					$sjo=$sjo.$sjc;
					next;
				}
	
			}
	
			### waiting for (
			if (($sjs==10) or ($sjs==11)) {
				if (($sjc eq ' ') or ($sjc eq "\t")) {
					$sjo=$sjo.$sjc;
					next;
				} elsif ($sjc eq '(') {
					if ($sjs==11) {
						if ($sjwr==0) {
							$sjo=$sjo.'(v3writer(';
							$sjwr=1;
						} else {$sjo=$sjo.'(v3write(';}
					} else {$sjo=$sjo.'('."\'".$jsr_location."\' + v3censor(";}
					$sjs=15;
					$sjb=1;
					next;
				} else {
					$sjo=$sjo.$sjc;
					$sjs=51;
					next;
				}
			}
	
			### waiting for end of bracket before resetting
			if ($sjs==15) {
				if ($sjc eq '(') {
					$sjo=$sjo.$sjc;
					$sjb++;
					next;
				} elsif (($sjc eq ')') and ($sjb>1)) {
					$sjo=$sjo.$sjc;
					$sjb--;
					next;
				} elsif (($sjb==1) and (($sjc eq ',') or ($sjc eq ')'))) {
						if ($sjc eq ')') {$sjb--;}
						$sjo=$sjo.')'.$sjc;
						$sjs=51;
						next;
				} else {
					$sjo=$sjo.$sjc;
					next;
				}
	
			}
	
		}
	
	}


	return $sjo;

}

###########

sub v3_print {
	$data_out=$data_out+length($_[0]);
	print $_[0];
}

###########

sub v3_datalog {

	if (lc($options{'logging'}) eq 'on') {

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);

		$ed_out=0;
		$ed_in=0;
		if (-e "$options{'log_dir'}$dub.data.log") {

			open (LOG,"$options{'log_dir'}$dub.data.log");
				@tails = <LOG>;
			close (LOG);
			foreach $tail (@tails) {
				($stn,$std) = split(' : ',$tail,2);
				if (lc($stn) eq 'in') {$ed_in=$std;}
				if (lc($stn) eq 'out') {$ed_out=$std;}
			}

		}
		$ed_out=$ed_out+$data_out;
		$ed_in=$ed_in+$data_in;

		open (LOG,">$options{'log_dir'}$dub.data.log");
			print LOG "IN : $ed_in\nOUT : $ed_out\n";
		close (LOG);
		&slog('@ Logged data flow.'."\n");
		chmod 0666,"$options{'log_dir'}$dub.data.log";

	}

}

###########

sub v3_banner_add {

	if (lc($options{'logging'}) eq 'on') {

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);

		$write_bc=0;
		$ed_in=0;
		if (-e "$options{'log_dir'}$dub.ads.log") {

			open (LOG,"$options{'log_dir'}$dub.ads.log");
				@tails = <LOG>;
			close (LOG);
			foreach $tail (@tails) {
				($stn,$std) = split(' : ',$tail,2);
				if (lc($stn) eq 'ads') {$ed_in=$std;}
			}

		}
		$write_bc=$ed_in+$ads_seen; ## NUMBER OF ADS SHOWN

		open (LOG,">$options{'log_dir'}$dub.ads.log");
			print LOG "ADS : $write_bc\n";
		close (LOG);
		chmod 0666,"$options{'log_dir'}$dub.ads.log";

	}

}

###########
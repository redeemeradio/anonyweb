#!/usr/bin/perl
#        _____ 
# __   _|___ / 
# \ \ / / |_ \ 
#  \ V / ___) |  E-Mail
#   \_/ |____/ 
#
# Version: 0.2 beta
#
# Jeremy Goodwin (nexcellent@hotmail.com)
#
##########################################

&GetFormInput;
&v3_options;

### prevent v3 looping
if (lc($ENV{'HTTP_USER_AGENT'}) eq lc($options{'user_agent'})) {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}\n\n";
	exit 0;
}

### LE fooling code
if ($ENV{'HTTP_USER_AGENT'} =~/LECodeChecker/ig) {
	print "Content-Type: text/html\n\n";
	print '<html><body bgcolor=#ffffff text=#000000 link=#000099>
<!-- BEGIN LINKEXCHANGE CODE --> 
<center><iframe src="http://leader.linkexchange.com/4/X1511758/showiframe?" width=468 height=60 marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>
<a href="http://leader.linkexchange.com/4/X1511758/clickle" target="_top"><img width=468 height=60 border=0 ismap alt="" src="http://leader.linkexchange.com/4/X1511758/showle?"></a></iframe><br><a href="http://leader.linkexchange.com/4/X1511758/clicklogo" target="_top"><img src="http://leader.linkexchange.com/4/X1511758/showlogo?" width=468 height=16 border=0 ismap alt=""></a><br></center>
<!-- END LINKEXCHANGE CODE -->
<br><br><font size=3 face=arial>Click
<a href="'.$options{'script_base_dir'}.$options{'script_name_v3_page'}.'" target="_top">here</a>
to continue!</font>
</body></html>';
	exit 0;
}

### send to no_email page if email has been disabled
if ($options{'allow_email'} ne 'on') {
	print "Location: $options{'script_base_dir'}$options{'script_name_v3_page'}?type=p&p=noemail\n\n";
}


$mail_prog = $options{'sendmail_loc'};

$to=$field{'to'};
$from=$field{'from'};
$subject=$field{'subject'};
$message=$field{'body'};

$iserror=0;

$bleh=$from;
$bleh=~s/\n//ig;
$bleh=~s/ //ig;
if ($bleh eq '') {
  $iserror=1;
  push @errmsg,"No 'from' e-mail address specified.";
} elsif (&check_email($from) eq false) {
	$iserror=1;
	push @errmsg,"Invalid 'from' e-mail address specified.";
}
$bleh=$to;
$bleh=~s/\n//ig;
$bleh=~s/ //ig;
if ($bleh eq '') {
  $iserror=1;
  push @errmsg,"No 'to' e-mail address specified.";
} else {
	$to=~s/\n/ /ig;
	$to=~s/,/ /ig;
	$to=~s/;/ /ig;
	@qa = split(' ',$to);
	my @ads = '';
	$cn=0;
	for ($l=0;$l<scalar(@qa);$l++) {
		if ($qa[$l] ne '') {
			if ($cn==0) {$eto=$qa[$l];
			} else {push @ads , $qa[$l];}
			$cn=1;
		}
	}
	if (&check_email($eto) eq false) {
		$iserror=1;
		push @errmsg,"Invalid 'to' e-mail address: '<b>$eto</b>' specified.";
	}
	$cc='';
	foreach $de (@ads) {
		if ($de eq '') {next;}
		if (&check_email($de) eq false) {
			$iserror=1;
			push @errmsg,"Invalid 'cc' e-mail address: '<b>$de</b>' specified.";
		} else {
			$cc=$cc.$de.', ';
		}
	}
	if ($cc ne '') {$cc=substr($cc,0,length($cc)-2);}
}
$bleh=$subject;
$bleh=~s/\n//ig;
$bleh=~s/ //ig;
if ($bleh eq '') {
  $iserror=1;
  push @errmsg,"No subject specified.";
}
$bleh=$message;
$bleh=~s/\n//ig;
$bleh=~s/ //ig;
if ($bleh eq '') {
  $iserror=1;
  push @errmsg,"No message specified.";
}

if ($iserror==0) {
	open (MAIL, "|$mail_prog -t");
		print MAIL "To: $eto\n";
		if ($cc ne '') {print MAIL "Cc: $cc\n";}
		print MAIL "Reply-to: $from\n";
		print MAIL "From: $from\n";
		print MAIL "Subject: $subject\n";
		print MAIL "Return-Path: $from\n";
		print MAIL "$message\n\n\n[This e-mail was sent using the proxy by-pass system v3.2, which can be found at http://vl8.net/whatever (where 'whatever' can be any random letters or numbers, but not nothing)]" ;
		print MAIL "\n\n";
	close (MAIL);

	## log email
	&email_log;

 	### print confirm page
	$head = "<b>To:</b> $eto<br>\n";
	if ($cc ne '') {$head = $head . "<b>Cc:</b> $cc<br>\n";}
	$head = $head . "<b>From:</b> $from<br>\n";
	$head = $head . "<b>Subject:</b> $subject<br>\n";

	$prme=$message;
	$prme=~s/>/&gt;/ig;
	$prme=~s/</&lt;/ig;
	$prme=~s/\n/<br>/ig;
	$prme="<br>\n".$prme;

	$peg = `$options{'local_base'}$options{'script_name_v3_page'} p email_sent`;
	$peg=~s/#\]header/$head/ig;
	$peg=~s/#\]body/$prme/ig;

	print "Content-Type: text/html\n\n";
	print $peg;

	exit 0;

} else {

	$out='';
	foreach $dat (@errmsg) {
		$out = $out . '<b>&#183;'." $dat</b><br>\n";
	}

	### print error page
	$peg = `$options{'local_base'}$options{'script_name_v3_page'} p email_error`;
	$peg=~s/#\]errors/$out/ig;	

	print "Content-Type: text/html\n\n";
	print $peg;

	exit 0;

}


exit 0;



#########################################

sub v3_options {
	open(SF,"v3-config/options");
	  @opf=<SF>;
	close(SF);
	foreach $opd (@opf) {
		$opd=~s/\r/ /ig;
		$opd=~s/\t/ /ig;
		$opn='';
		$opc='';
		$ops=0;
		for ($opz=0;$opz<length($opd);$opz++) {
			$ope=substr($opd,$opz,1);
			if ($ope eq '#') {last;}
			if (($ops==0) and ($ope ne ' ')) {
				$opn=$ope;
				$ops=1;
				next;
			}
			if ($ops==1) {
				if ($ope eq ' ') {$ops=2;
				} else {$opn=$opn.$ope;}
				next;
			}
			if ($ops==2) {
				if (($ope ne ' ') and ($ope ne '=')) {
					$opc=$ope;
					$ops=3;
					next;
				}
			}
			if ($ops==3) {
				if ($ope eq ';') {
					$ops=4;
					last;
				} else {$opc=$opc.$ope;}
			}
		}
		$opn=lc($opn);
		if ($ops==4) {
			$options{$opn}=$opc;
		}
	}
}

###########

sub GetFormInput {

	(*fval) = @_ if @_ ;

	local ($buf);
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN,$buf,$ENV{'CONTENT_LENGTH'});
	}
	else {
		$buf=$ENV{'QUERY_STRING'};
	}
	if ($buf eq "") {
			return 0 ;
		}
	else {
 		@fval=split(/&/,$buf);
		foreach $i (0 .. $#fval){
			($name,$val)=split (/=/,$fval[$i],2);
			$val=~tr/+/ /;
			$val=~ s/%(..)/pack("c",hex($1))/ge;
			$name=~tr/+/ /;
			$name=~ s/%(..)/pack("c",hex($1))/ge;

			if (!defined($field{$name})) {
				$field{$name}=$val;
			}
			else {
				$field{$name} .= ",$val";
				
			
			}


		   }
		}
return 1;
}

###########

sub check_email {
	$addr = $_[0];
	if ($addr eq '') {return false;}
	@at = split ('@',$addr);
	if (scalar(@at) < 2) {return false;}
	if ($at[0] eq '') {return false;}
	if ($at[1] eq '') {return false;}
	$at[1]=~s/\./;:/ig;
	@dt = split (';:',$at[1]);
	if (scalar(@dt) < 2) {return false;}
	foreach $ft (@dt) {
		if ($ft eq '') {return false;}
	}
	$tmp = $addr;
	$tmp =~ s/[0-9]//gi;
	$tmp =~ s/[a-z]//gi;
	$tmp =~ s/[A-Z]//gi;
	$tmp =~ s/\-//gi;
	$tmp =~ s/\%//gi; 
	$tmp =~ s/\_//gi;
	$tmp =~ s/\@//gi;  
	$tmp =~ s/\.//gi;
	if ($tmp ne '') {return false;}
	return true;
}

###########

sub email_log {

	if (lc($options{'logging'}) eq 'on') {

		$ll[0] = $eto;
		$ll[1] = $cc;
		$ll[2] = $from;
		$ll[3] = $subject;
		$ll[4] = $message;

		for ($z=0;$z<scalar(@ll);$z++) {
			$ll[$z]=~s/;/:/ig;
			$ll[$z]=~s/\n/\\n/ig;
			$ll[$z]=~s/\r//ig;
		}

		$dub = `date +%d-%m-%Y`;
		chomp ($dub);
		$tim = `date +%T`;
		chomp ($tim);

		$logline = "$tim ;>> User_IP;: $ENV{'REMOTE_ADDR'} ; User_Host;: $ENV{'REMOTE_HOST'} ; User_UA;: $ENV{'HTTP_USER_AGENT'} ; To;: $ll[0] ; Cc;: $ll[1] ; From;: $ll[2] ; Subject;: $ll[3] ; Body;: $ll[4]";
		$logline=~s/\n/\\n/ig;
		$logline=~s/\r//ig;
		$logline=~s/<\/pre>/<\\\/pre>/ig;

		if (-e "$options{'log_dir'}$dub.email.log") {

			open (LOG,">>$options{'log_dir'}$dub.email.log");
				print LOG "$logline\n";
			close (LOG);

		} else {

			$fulldate = `date +%A`;
			chomp ($fulldate);
			$fulldate = $fulldate . ', ' . `date +%d-%m-%Y`;
			chomp ($fulldate);
			open (LOG,">$options{'log_dir'}$dub.email.log");
				print LOG "# v3-log (email) > $fulldate\n";
				print LOG "$logline\n";
			close (LOG);
			chmod 0666,"$options{'log_dir'}$dub.email.log";

		}

	}

}

###########